import BurgerBuilder from './containers/BurgeBuilder/BurgerBuilder';
import Layout from './containers/Layout/Layout';

import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <BurgerBuilder />
        </Layout>
      </div>
    );
  }
}

export default App;