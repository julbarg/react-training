/**
 * Destructuring
 * Easily extract array elements or objects properties and store in variables
 */

// Array Destructuring
[a, b] = ['Hello', 'Max'];
console.log(a); // Hello
console.log(b); // Max

// Object Destructuring
{name} = {name: 'Max', age: 28};
console.log(name);  // Max
console.log(age); // undefined

const numbers = [1, 2, 3];
[num1, num2] = numbers;
console.log(num1, num2); // 1 2




