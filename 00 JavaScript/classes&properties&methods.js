/**
 * Properties are like variables attached to classes/objects
 *
 * Methods are like functions attached to classes/objects
 */

class Some {
    constructor() {
        this.myProperty = 'value';
    }

    myMethod() {

    }
}

myProperty = 'value';
myMethod = () => {}


/**
 * ES7
 */
class Human {
    gender = 'male';

    printGender = () => {
         console.log(this.gender);
    }
}

class Person extends Human {
    name = 'Max';
    gender = 'female';

    printMyName = () => {
        console.log(this.name);
    }
}


