function myFunction () {

}

const myFunction = () => {

}

function printMyName (name) {
    console.log(name);
}

printMyNameArrowFunction('Julian');

const printMyName = name => {
    console.log(name);
}

printMyNameArrowFunction('JulianB');

const print = () => {
    console.log('Julian');
}

const printer = (name, age) => {
    console.log(name, age);
}

const multiply = number => {
    return number * 2;
}

console.log(multiply(2));

const multiply = number => number * 2;

console.log(multiply(2));

