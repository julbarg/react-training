class Person {
    constructor() {
        this.name = 'Julian';
    }

    printMyName() {
        console.log(this.name);
    }

    name = 'Max';
    call = () => {

    };
}

const myPerson = new Person();
myPerson.call();

class Person extends Master {

}

class Human {
    constructor() {
        this.gender = 'male';
    }

    printGender() {
        console.log(this.gender);
    }
}

class Person extends Human {
    constructor() {
        super();
        this.name = 'Max';
        this.gender = 'female';
    }

    printMyName() {
        console.log(this.name);
    }
}

const person = new Person();
person.printMyName();
person.printGender();

