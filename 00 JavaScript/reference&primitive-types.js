/**
 * Reference and Primitive Type
 */
const number = 1;
const num2 = number;

num2 = 12;

console.log(num2);
console.log(number);

const person = {
    name: 'Max'
};

const secondPerson = person;

const secondPerson = {
    ...person
}

console.log(secondPerson);

// Object copy as a reference
// Primitive is a copy
