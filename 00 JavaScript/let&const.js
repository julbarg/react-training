/**
 * var
 * let variable values
 * const constant values
 */
var myName = 'Max';
console.log(myName);

myName = 'Manu';
console.log(myName);

const name = 'Julian';
name = 'JulianB'; // Assignment to constant variable

