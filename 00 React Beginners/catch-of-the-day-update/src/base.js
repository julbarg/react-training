
import * as firebase from 'firebase/app';
import Rebase from 're-base'


import 'firebase/database';

const config = {
    apiKey: "AIzaSyDgWS69Os0q_AA123e2Vl6Sma98yFk2c7M",
    authDomain: "catch-of-the-day-julian-74efb.firebaseapp.com",
    databaseURL: "https://catch-of-the-day-julian-74efb.firebaseio.com",
}

const app = firebase.initializeApp(config)
const base = Rebase.createClass(app.database())

export { app, base }