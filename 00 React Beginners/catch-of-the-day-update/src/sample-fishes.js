// This is just some sample data so you don't have to think of your own!
module.exports = {
  fish1: {
    name: 'Pacific Halibut',
    image: 'https://media.istockphoto.com/photos/mixed-seafood-on-ice-background-4x5-film-picture-id94923745',
    desc: 'Everyones favorite white fish. We will cut it to the size you need and ship it.',
    price: 1724,
    status: 'available'
  },

  fish2: {
    name: 'Lobster',
    image: 'https://media.istockphoto.com/photos/close-up-of-fresh-steamed-lobster-with-herbs-in-grey-plate-picture-id117149629',
    desc: 'These tender, mouth-watering beauties are a fantastic hit at any dinner party.',
    price: 3200,
    status: 'available'
  },

  fish3: {
    name: 'Sea Scallops',
    image: 'https://media.istockphoto.com/photos/delicacies-fresh-scallop-mussel-at-fishing-net-picture-id954475258',
    desc: 'Big, sweet and tender. True dry-pack scallops from the icey waters of Alaska. About 8-10 per pound',
    price: 1684,
    status: 'unavailable'
  },

  fish4: {
    name: 'Mahi Mahi',
    image: 'https://media.istockphoto.com/photos/robalo-grelhado-com-legumes-2-picture-id949159616',
    desc: 'Lean flesh with a mild, sweet flavor profile, moderately firm texture and large, moist flakes. ',
    price: 1129,
    status: 'available'
  },

  fish5: {
    name: 'King Crab',
    image: 'https://media.istockphoto.com/photos/king-crab-plate-picture-id174866241',
    desc: 'Crack these open and enjoy them plain or with one of our cocktail sauces',
    price: 4234,
    status: 'available'
  },

  fish6: {
    name: 'Atlantic Salmon',
    image: 'https://media.istockphoto.com/photos/salmon-fillet-portions-selling-in-supermarket-picture-id1016589316',
    desc: 'This flaky, oily salmon is truly the king of the sea. Bake it, grill it, broil it...as good as it gets!',
    price: 1453,
    status: 'available'
  },

  fish7: {
    name: 'Oysters',
    image: 'https://media.istockphoto.com/photos/fresh-tasty-open-oysters-picture-id974109388',
    desc: 'A soft plump oyster with a sweet salty flavor and a clean finish.',
    price: 2543,
    status: 'available'
  },

  fish8: {
    name: 'Mussels',
    image: 'https://media.istockphoto.com/photos/bowl-of-mussels-picture-id1145861964',
    desc: 'The best mussels from the Pacific Northwest with a full-flavored and complex taste.',
    price: 425,
    status: 'available'
  },

  fish9: {
    name: 'Jumbo Prawns',
    image: 'https://media.istockphoto.com/photos/jumbo-tiger-prawn-scampi-picture-id984444796',
    desc: 'With 21-25 two bite prawns in each pound, these sweet morsels are perfect for shish-kabobs.',
    price: 2250,
    status: 'available'
  }
};
