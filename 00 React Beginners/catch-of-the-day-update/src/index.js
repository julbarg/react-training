import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import StorePicker from './components/StorePicker'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import App from './components/App';
import NotFound from './components/NotFound';
import './css/style.css';

const Root = () => {
    return (
        <Router>
            <Switch>
                <Route exact path='/' component={StorePicker}/>
                <Route path='/store/:storeId' component={App} />
                <Route component={NotFound} />
            </Switch>
        </Router>
    )
}

ReactDOM.render(<Root />, document.getElementById('main'));