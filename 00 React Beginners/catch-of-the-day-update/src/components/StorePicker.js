import React from 'react';
import { getFunName } from '../helpers';


class StorePicker extends React.Component {
    // constructor () {
    //     super();
    //     this.goToStore = this.goToStore.bind(this)
    // }

    goToStore (e) {
        e.preventDefault();
        // first gran the text from the box
        const storeId = this.storeInput.value;
        // second we're going to transition from / to /store/:storeId
        this.props.history.push({
            pathname: `store/${storeId}`,
            state: { storeId: storeId }
          })
    }

    render () {
        // Another comment
        // <form className="store-selector" onSubmit={this.goToStore.bind(this)}>
        return (
            <form className="store-selector" onSubmit={(e) => this.goToStore(e)}>
                { /* Comments */ }
                <h2>Please Enter a Store</h2>
                <input
                    type="text"
                    required
                    placeholder="Store Name" defaultValue={getFunName()}
                    ref={(input) => { this.storeInput = input}}/>
                <button type="submit">Visit Store -></button>
            </form>
        );
    }
}

export default StorePicker;