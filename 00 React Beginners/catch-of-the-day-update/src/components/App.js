import { base } from '../base'
import Fish from './Fish';
import Header from './Header';
import Inventory from './Inventory';
import Order from './Order';
import React from 'react';
import sampleFishes from '../sample-fishes';
import PropTypes from 'prop-types';

class App extends React.Component {
    constructor () {
        super();

        // this.loadSamples = this.loadSamples.bind(this);

        this.addFish = this.addFish.bind(this);
        // this.updateFish = this.updateFish.bind(this);
        // this.removeFish = this.removeFish.bind(this);

        this.addToOrder = this.addToOrder.bind(this);
        this.removeFromOrder = this.removeFromOrder.bind(this);

        // getInitialState
        // this.state = {
        //     fishes: {},
        //     order: {}
        // };
    }

    state = {
        fishes: {},
        order: {}
    };

    

    render () {
        return (
            <div className="catch-of-the-day">
                <div className="menu">
                    <Header tagline="Fresh Seafood Market"/>
                    <ul className="list-of-fishes">
                        {
                            Object
                                .keys(this.state.fishes)
                                .map(key => <Fish key={key} index={key} details={this.state.fishes[key]} addToOrder={this.addToOrder}/>)
                        }
                    </ul>
                </div>
                <Order
                    params={this.props.match.params}
                    fishes={this.state.fishes}
                    order={this.state.order}
                    removeFromOrder={this.removeFromOrder}
                />
                <Inventory
                    addFish={this.addFish}
                    loadSamples={this.loadSamples}
                    fishes={this.state.fishes}
                    updateFish={this.updateFish}
                    removeFish={this.removeFish}
                    storeId={this.props.match.params.storeId}
                />
            </div>
        );
    }

    componentDidMount() {
        const storeId = this.props.match.params.storeId;
        this.ref = base.syncState(`${storeId}/fishes`, {
            context: this,
            state: 'fishes'
        });

        // check if there is any order in localStorage
        const localStorageRef = localStorage.getItem(`order-${this.props.match.params.storeId}`);

        if(localStorageRef) {
            // update our App component's order state
            this.setState({
                order: JSON.parse(localStorageRef)
            });
        }
    }

    componentDidUpdate(nextProps, nextState) {
         localStorage.setItem(
             `order-${this.props.match.params.storeId}`,
             JSON.stringify(nextState.order)
         );
    }

    componentWillUnmount() {
        base.removeBinding(this.ref);
    }

    addFish(fish) {
        // update our state
        const fishes = {...this.state.fishes};
        // add in our new fish
        const timestamp = Date.now();
        fishes[`fish-${timestamp}`] = fish;
        // set state
        this.setState({ fishes });
    }

    updateFish = (key, updatedFish) => {
        const fishes = {...this.state.fishes};
        fishes[key] = updatedFish;
        this.setState({
            fishes
        });
    }

    removeFish = (key) => {
        const fishes = {...this.state.fishes};
        fishes[key] = null;
        this.setState({
            fishes
        });
    }

    loadSamples = () => {
        this.setState({
            fishes: sampleFishes
        });
    };

    addToOrder(key) {
        // take a copy of our state
        const order = {...this.state.order};
        // update or add the new number of fish order
        order[key] = order[key] + 1 || 1;
        this.setState({ order });
    }

    removeFromOrder(key) {
        const order = {...this.state.order};
        delete order[key];
        this.setState({ order });
    }
}

App.propTypes = {
    match: PropTypes.object.isRequired
}

export default App;