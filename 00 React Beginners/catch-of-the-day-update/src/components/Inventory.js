import { app, base } from '../base';
import AddFishForm from './AddFishForm';
import PropTypes from 'prop-types';
import React from 'react';
import * as firebase from 'firebase/app';
import "firebase/auth";


class Inventory extends React.Component {

    constructor() {
        super();
        this.renderInventory = this.renderInventory.bind(this);
        this.renderLogin = this.renderLogin.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.authenticate = this.authenticate.bind(this);
        this.authHandler = this.authHandler.bind(this);
        this.logout = this.logout.bind(this);
        this.state = {
            owner: null,
            uid: null
        }
    }

    render () {
        const logout = <button onClick={this.logout}>Log Out!</button>;
        // Check if they are no logged in at all
        if (!this.state.uid) {
            return <div>{this.renderLogin()}</div>
        }

        // Check if the are the owner of the current store
        if (this.state.uid !== this.state.owner) {
            return (
                <div>
                    <p>Sorry you aren't the owner of this store</p>
                    {logout}
                </div>
            )
        }
        return (
            <div>
                <h2>Inventory</h2>
                {logout}
                {Object.keys(this.props.fishes).map(this.renderInventory)}
                <AddFishForm addFish={this.props.addFish} />
                <button onClick={this.props.loadSamples}>Load Sample Fishes</button>
            </div>
        );
    }

    renderInventory(key) {
        const fish = this.props.fishes[key];
        return (
            <div className="fish-edit" key={key}>
                <input type="text" name="name" value={fish.name}  placeholder="Fish Name" onChange={(e) => this.handleChange(e, key)} />
                <input type="text" name="price" value={fish.price} placeholder="Fish Price" onChange={(e) => this.handleChange(e, key)} />

                <select type="text" name="status" value={fish.status} placeholder="Fish Status" onChange={(e) => this.handleChange(e, key)}>
                    <option value="available">Fresh!</option>
                    <option value="unavailable">Sold Out</option>
                </select>

                <textarea type="text" name="desc" value={fish.desc} placeholder="Fish Desc" onChange={(e) => this.handleChange(e, key)}>
                </textarea>
                <input type="text" name="image" value={fish.image} placeholder="Fish Image"onChange={(e) => this.handleChange(e, key)} />
                <button onClick={() => this.props.removeFish(key)}>Remove</button>

            </div>
        );
    }

    renderLogin() {
        return (
            <nav className="login">
                <h2>Inventory</h2>
                <p>Sign in to manage your store's inventory</p>
                <button className="github" onClick={() => this.authenticate('github')}>Log In with Github</button>
                <button className="facebook" onClick={() => this.authenticate('facebook')}>Log In with Facebook</button>
                <button className="twitter" onClick={() => this.authenticate('twitter')}>Log In with Twitter</button>
            </nav>
        )
    }

    componentDidMount() {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.authHandler({ user });
            }
        });
    }

    authenticate(provider) {
        var authProvider;

        switch (provider) {
            case 'facebook':
                authProvider = new firebase.auth.FacebookAuthProvider();
                break;
            case 'twitter':
                authProvider = new firebase.auth.TwitterAuthProvider();
                break;
            case 'github':
                authProvider = new firebase.auth.GithubAuthProvider();
                break;
            default:
                authProvider = null;
                break;
        }
        if (authProvider) {
            app.auth().signInWithPopup(authProvider)
                .then(this.authHandler)
                .catch((error) => {
                    console.error(error);
                    return;
                });

            //
        }
    }

    authHandler(authData) {
        const storeRef = app.database().ref(this.props.storeId);
        // query the firebase once fr the store data
        storeRef.once('value', (snapshot) => {
            const data = snapshot.val() || {};

            // claim it as our own if there is no owner already
            if (!data.owner) {
                storeRef.set({
                    owner: authData.user.uid
                })
            }

            this.setState({
                owner: data.owner || authData.user.uid,
                uid: authData.user.uid
            });
        })
    }

    logout() {
        firebase.auth().signOut();
        this.setState({ uid: null });
    }

    handleChange(e, key) {
        const fish = this.props.fishes[key];
        // take the copy of that fish and update it with the new data
        const updatedFish = {
            ...fish,
            [e.target.name]: e.target.value
        };
        this.props.updateFish(key, updatedFish);
    }

    static propTypes = {
        fishes: PropTypes.object.isRequired,
        updateFish: PropTypes.func.isRequired,
        removeFish: PropTypes.func.isRequired,
        addFish: PropTypes.func.isRequired,
        loadSamples: PropTypes.func.isRequired,
        storeId: PropTypes.string.isRequired
    }
}

// Inventory.propTypes = {
//     fishes: PropTypes.object.isRequired,
//     updateFish: PropTypes.func.isRequired,
//     removeFish: PropTypes.func.isRequired,
//     addFish: PropTypes.func.isRequired,
//     loadSamples: PropTypes.func.isRequired,
//     storeId: PropTypes.string.isRequired
// }

export default Inventory;