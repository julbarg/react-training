import React from 'react';
import { getFunName } from '../helpers';

class StorePicker extends React.Component {
    // constructor () {
    //     super();
    //     this.goToStore = this.goToStore.bind(this)
    // }
    constructor(props, context) {
      super(props, context);
    }

    goToStore (e) {
        e.preventDefault();
        // first gran the text from the box
        const storeId = this.storeInput.value;
        // second we're goind to transition from / to /store/:storeId
        console.log();
        // this.context.router.replaceWith(`/store/${storeId}`);
        this.props.history.push(`store/${storeId}`);
    }

    render () {
        // Another comment
        // <form className="store-selector" onSubmit={this.goToStore.bind(this)}>
        return (
            <form className="store-selector" onSubmit={(e) => this.goToStore(e)}>
                { /* Comments */ }
                <h2>Please Enter a Store</h2>
                <input
                    type="text"
                    required
                    placeholder="Store Name" defaultValue={getFunName()}
                    ref={(input) => { this.storeInput = input}}/>
                <button type="submit">Visit Store -></button>
            </form>
        );
    }
}

StorePicker.contextTypes = {
  router: function contextType() {
    return React.PropTypes.func.isRequired;
  }
};

export default StorePicker;