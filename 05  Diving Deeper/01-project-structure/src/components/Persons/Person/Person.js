import React from 'react';
import classes from './Person.css';

const Person = ( props ) => {
    console.log('[Person.js] rendering...');
    return (
        <div className={classes.Person}>
            <p onClick={props.click}>I'm a {props.name} I am {props.age} years old!</p>
            <p>{props.children}</p>
            <input value={props.name} type="text" onChange={props.changed}/>
        </div>
    );
};

export default Person;