import React from 'react';

const WithClassWrappedComponent = (WrappedComponent, className) => {
    return props => (
        <div className={className}>
            <WrappedComponent {...props} />
        </div>

    )
}

export default WithClassWrappedComponent;

/**
    <Aux>
        <Person />
    </Aux>

    export default WithClass(App, classes.App);
 */