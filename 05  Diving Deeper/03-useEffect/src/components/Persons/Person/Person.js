
// import Aux from "../../../hoc/Aux";
import AuthContext from '../../../context/auth-context';
import classes from "./Person.css";
import PropTypes from 'prop-types';
import React, { Component, Fragment } from "react";
import WithClassWrappedComponent from '../../../hoc/WithClassWrappedComponent';

class Person extends Component {
  constructor(props) {
    super(props);
    this.inputElementRef = React.createRef();
  }

  componentDidMount() {
    // this.inputElement.focus();
    this.inputElementRef.current.focus();
  }

  render() {
    console.log("[Person.js] rendering...");

    return (
      // <div className={classes.Person}>
      // <Aux>
      <Fragment className={classes.Person}>
        <AuthContext.Consumer>
          {(context) => context.authenticated ? <p>Authenticated</p> : <p>Please log in!</p>}
        </AuthContext.Consumer>
        <p onClick={this.props.click}>
          I'm a {this.props.name} I am {this.props.age} years old!
        </p>
        <p>{this.props.children}</p>
        <input
          // ref={(inputEl) => {this.inputElement = inputEl}}
          ref = {this.inputElementRef}
          value={this.props.name}
          type="text"
          onChange={this.props.changed}
        />
      </Fragment>
      // </Aux>
      // </div>
    );
    /* Rendering Adjacent JSX Elements */

    // return [
    //     <p key="i1" onClick={this.props.click}>
    //       I'm a {this.props.name} I am {this.props.age} years old!
    //     </p>,
    //     <p key="i2">{this.props.children}</p>,
    //     <input key="i3" value={this.props.name} type="text" onChange={this.props.changed} />
    // ];
  }
}

Person.propTypes = {
  age: PropTypes.number,
  changed: PropTypes.func,
  click: PropTypes.func,
  name: PropTypes.string
};

export default WithClassWrappedComponent(Person, classes.Person);
