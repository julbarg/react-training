import React from 'react';
import './Person.css';

const Person = ( props ) => {
    // return <p>I'm a Person I am {Math.floor(Math.random() * 30)} years old!</p>;
    return (
        <div className="Person">
            <p onClick={props.click}>I'm a {props.name} I am {props.age} years old!</p>
            <p>{props.children}</p>
            <input value={props.name} type="text" onChange={props.changed}/>
        </div>
    );
};

export default Person;