const counter  = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state -1;
        default:
            return state;
    }
}

const Counter = ({ value, onIncrement, onDecrement }) => (
    <div>
        <h1>{value}</h1>
        <button onClick={onIncrement}>+</button>
        <button onClick={onDecrement}>-</button>
    </div>
);

const { createStore } = Redux;
// import { createStore } from 'redux';

// specify the reducer
// counter is the reducer that manges the state updates
const store = createStore(counter);

console.log(store.getState());

// Dispatch: It lets you dispatch actions to change the state of your application
// store.dispatch({ type: 'INCREMENT'});

console.log(store.getState());

// We're actually going to render something to the body with the help of the third Redux chore method,
// called subscribe. It lets you register a callback that the Redux chore will call any time an action
// has been dispatch, so that you can update the UI of your application. It will reflect the current
// application state
const render = () => {
    ReactDOM.render(
        <Counter
            value={store.getState()}
            onIncrement={() => {
                store.dispatch({
                    type: 'INCREMENT'
                })
            }}
            onDecrement={() => {
                store.dispatch({
                    type: 'DECREMENT'
                })
            }}
        />,
        document.getElementById('root')
    );
}

/*
    Adds a change listener. It will be called any time an action is dispatched, and some part of the state tree may potentially have changed.
*/
store.subscribe(render);
render();

document.addEventListener('click', () => {
    store.dispatch({ type: 'INCREMENT' })
});