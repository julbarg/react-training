const counter = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        default:
            return state;
    }
};

const todo = (state, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                id: action.id,
                text: action.text,
                completed: false,
            };
        case 'TOGGLE_TODO':
            if (state.id !== action.id) {
                return state;
            }

            return {
                ...state,
                completed: !state.completed,
            };
        default:
            return state;
    }
};

const todos = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return [...state, todo(undefined, action)];
        case 'TOGGLE_TODO':
            return state.map(t => {
                let ex = todo(t, action);
                console.log('Example', ex);
                return ex;
            });
        default:
            return state;
    }
};

const addCounter = list => {
    return [...list, 0];
};

const removeCounter = (list, index) => {
    return [...list.slice(0, index), ...list.slice(index + 1)];
};

const incrementCounter = (list, index) => {
    return [...list.slice(0, index), list[index] + 1, ...list.slice(index + 1)];
};

const toggleTodo = todo => {
    return {
        ...todo,
        completed: !todo.completed,
    };
};

const visibilityFilter = (state = 'SHOW_ALL', action) => {
    switch (action.type) {
        case 'SET_VISIBILITY_FILTER':
            return action.filter;
        default:
            return state;
    }
};

const { combineReducers } = Redux;
const todoApp = combineReducers({
    todos: todos,
    visibilityFilter: visibilityFilter,
});

/* const todoApp = (state = {}, action) => {
return {
  todos: todos(state.todos, action),
  visibilityFilter: visibilityFilter(state.visibilityFilter, action)
}
}*/

// const removeCounter = (list, index) => {
//   return [
//     ...list.slice(0, index),
//     ...list.slice(index + 1)
//   ]
// }

// const Counter = ({ value, onIncrement, onDecrement }) => (
//     <div>
//         <h1>{value}</h1>
//         <button onClick={onIncrement}>+</button>
//         <button onClick={onDecrement}>-</button>
//     </div>
// );

const { createStore } = Redux;
// import { createStore } from 'redux';

// specify the reducer
// counter is the reducer that manges the state updates
// const store = createStore(counter);
const store = createStore(todoApp);
console.log(store.getState());

store.dispatch({
    type: 'ADD_TODO',
    id: 0,
    text: 'Learn Redux',
});

console.log(store.getState());

store.dispatch({
    type: 'ADD_TODO',
    id: 1,
    text: 'Go shopping',
});

console.log(store.getState());

/*store.dispatch({
type: 'TOGGLE_TODO',
id: 0
});*/
store.dispatch({
    type: 'TOGGLE_TODO',
    id: 1,
});

console.log(store.getState());

store.dispatch({
    type: 'SET_VISIBILITY_FILTER',
    filter: 'SHOW_COMPLETED',
});

console.log(store.getState());

/*console.log(store.getState());

// Dispatch: It lets you dispatch actions to change the state of your application
// store.dispatch({ type: 'INCREMENT'});

console.log(store.getState());

// We're actually going to render something to the body with the help of the third Redux chore method,
// called subscribe. It lets you register a callback that the Redux chore will call any time an action
// has been dispatch, so that you can update the UI of your application. It will reflect the current
// application state
const render = () => {
ReactDOM.render(
    <Counter
        value={store.getState()}
        onIncrement={() => {
            store.dispatch({
                type: 'INCREMENT'
            })
        }}
        onDecrement={() => {
            store.dispatch({
                type: 'DECREMENT'
            })
        }}
    />,
    document.getElementById('root')
);
}

/*
Adds a change listener. It will be called any time an action is dispatched, and some part of the state tree may potentially have changed.
*/
/*store.subscribe(render);
render();*/
