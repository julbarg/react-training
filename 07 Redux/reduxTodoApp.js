const counter = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        default:
            return state;
    }
};

const todo = (state, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                id: action.id,
                text: action.text,
                completed: false,
            };
        case 'TOGGLE_TODO':
            if (state.id !== action.id) {
                return state;
            }

            return {
                ...state,
                completed: !state.completed,
            };
        default:
            return state;
    }
};

const todos = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return [...state, todo(undefined, action)];
        case 'TOGGLE_TODO':
            return state.map(t => {
                let ex = todo(t, action);
                console.log('Example', ex);
                return ex;
            });
        default:
            return state;
    }
};

const addCounter = list => {
    return [...list, 0];
};

const removeCounter = (list, index) => {
    return [...list.slice(0, index), ...list.slice(index + 1)];
};

const incrementCounter = (list, index) => {
    return [...list.slice(0, index), list[index] + 1, ...list.slice(index + 1)];
};

const toggleTodo = todo => {
    return {
        ...todo,
        completed: !todo.completed,
    };
};

const visibilityFilter = (state = 'SHOW_ALL', action) => {
    switch (action.type) {
        case 'SET_VISIBILITY_FILTER':
            return action.filter;
        default:
            return state;
    }
};

const { combineReducers } = Redux;

const todoApp = combineReducers({
    todos,
    visibilityFilter,
});

/* const todoApp = (state = {}, action) => {
  return {
    todos: todos(state.todos, action),
    visibilityFilter: visibilityFilter(state.visibilityFilter, action)
  }
}*/

// const removeCounter = (list, index) => {
//   return [
//     ...list.slice(0, index),
//     ...list.slice(index + 1)
//   ]
// }

// const Counter = ({ value, onIncrement, onDecrement }) => (
//     <div>
//         <h1>{value}</h1>
//         <button onClick={onIncrement}>+</button>
//         <button onClick={onDecrement}>-</button>
//     </div>
// );

const { createStore } = Redux;
// import { createStore } from 'redux';

// specify the reducer
// counter is the reducer that manges the state updates
// const store = createStore(counter);
const store = createStore(todoApp);
console.log(store.getState());

/*store.dispatch({
    type: 'ADD_TODO',
    id: 0,
    text: 'Learn Redux',
});

console.log(store.getState());

store.dispatch({
    type: 'ADD_TODO',
    id: 1,
    text: 'Go shopping',
});*/

console.log(store.getState());

/*store.dispatch({
type: 'TOGGLE_TODO',
id: 0
});*/
store.dispatch({
    type: 'TOGGLE_TODO',
    id: 1,
});

console.log(store.getState());

/*store.dispatch({
  type: 'SET_VISIBILITY_FILTER',
  filter: 'SHOW_COMPLETED'
});*/

console.log(store.getState());

/*console.log(store.getState());

// Dispatch: It lets you dispatch actions to change the state of your application
// store.dispatch({ type: 'INCREMENT'});

console.log(store.getState());

// We're actually going to render something to the body with the help of the third Redux chore method,
// called subscribe. It lets you register a callback that the Redux chore will call any time an action
// has been dispatch, so that you can update the UI of your application. It will reflect the current
// application state
const render = () => {
  ReactDOM.render(
      <Counter
          value={store.getState()}
          onIncrement={() => {
              store.dispatch({
                  type: 'INCREMENT'
              })
          }}
          onDecrement={() => {
              store.dispatch({
                  type: 'DECREMENT'
              })
          }}
      />,
      document.getElementById('root')
  );
}

/*
  Adds a change listener. It will be called any time an action is dispatched, and some part of the state tree may potentially have changed.
*/
/*store.subscribe(render);
render();*/
const { Component } = React;

const FilterLink = ({ filter, currentFilter, children }) => {
    if (filter === currentFilter) {
        return (
            <span>
                {children}
            </span>
        );
    }

    return (
        <a
            href="#"
            onClick={e => {
                e.preventDefault();
                store.dispatch({
                    type: 'SET_VISIBILITY_FILTER',
                    filter,
                });
            }}
        >
            {children}
        </a>
    );
};

const AddTodo = ({ onAddClick }) => {
    let input;

    return (
        <div>
            <input
                ref={node => {
                    input = node;
                }}
            />
            <button
                onClick={() => {
                    onAddClick(input.value);
                    input.value = '';
                }}
            >
                Add Todo
            </button>
        </div>
    );
};

const Footer = ({visibilityFilter}) => (
  <p>
        Show: {' '}
        <FilterLink filter="SHOW_ALL" currentFilter={visibilityFilter}>
            All
        </FilterLink>{' '}
        <FilterLink filter="SHOW_ACTIVE" currentFilter={visibilityFilter}>
            Active
        </FilterLink>{' '}
        <FilterLink filter="SHOW_COMPLETED" currentFilter={visibilityFilter}>
            Completed
        </FilterLink>
    </p>
);

const getVisibleTodos = (todos, filter) => {
    switch (filter) {
        case 'SHOW_ALL':
            return todos;
        case 'SHOW_COMPLETED':
            return todos.filter(t => t.completed);
        case 'SHOW_ACTIVE':
            return todos.filter(t => !t.completed);
    }
};

const Todo = ({ onClick, completed, text }) =>
    <li
        onClick={onClick}
        style={{
            textDecoration: completed ? 'line-through' : 'none',
        }}
    >
        {text}
    </li>;

const TodoList = ({ todos, onTodoClick }) =>
    <ul>
        {todos.map(todo =>
            <Todo
                key={todo.id}
                {...todo}
                onClick={() => onTodoClick(todo.id)}
            />
        )}
    </ul>;

let nextTodoId = 0;
class TodoApp extends Component {
    render() {
        const { todos, visibilityFilter } = this.props;
        const visibleTodos = getVisibleTodos(todos, visibilityFilter);
        return (
            <div>
                <AddTodo
                    onAddClick={text =>
                        store.dispatch({
                            type: 'ADD_TODO',
                            id: nextTodoId++,
                            text,
                        })}
                />
                <TodoList
                    todos={visibleTodos}
                    onTodoClick={id =>
                        store.dispatch({
                            type: 'TOGGLE_TODO',
                            id,
                        })}
                />
                <Footer {visibilityFilter} />
            </div>
        );
    }
}

const render = () => {
    ReactDOM.render(
        <TodoApp {...store.getState()} />,
        document.getElementById('root')
    );
};

store.subscribe(render);
render();
