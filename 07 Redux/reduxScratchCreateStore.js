const counter  = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state -1;
        default:
            return state;
    }
}

//const { createStore } = Redux;
const createStore = (reducer) => {
    let state;
    let listeners = []:

    const getState = () => state;

    const dispatch = (action) => {
        state = reducer(state, action);
        listeners.forEach(listener => listener());
    };

    const subscribe = (listener) => {
        listeners.push(listener);
        return () => {
            listeners = listeners.filter(l => l !== listener);
        }
    };

    dispatch({});

    return { getState, dispatch, subscribe };
}
// import { createStore } from 'redux';

// specify the reducer
// counter is the reducer that manges the state updates
const store = createStore(counter);

console.log(store.getState());

// Dispatch: It lets you dispatch actions to change the state of your application
// store.dispatch({ type: 'INCREMENT'});

console.log(store.getState());

// We're actually going to render something to the body with the help of the third Redux chore method,
// called subscribe. It lets you register a callback that the Redux chore will call any time an action
// has been dispatch, so that you can update the UI of your application. It will reflect the current
// application state
const render = () => {
    document.body.innerText = store.getState();
}

store.subscribe(render);
render();

document.addEventListener('click', () => {
    store.dispatch({ type: 'INCREMENT' })
});