import React, { useState, useEffect } from 'react';

const Counter = () => {
	const [counter, setCounter] = useState(0);

	useEffect (() => {
		console.log(counter);
	}, [counter]);

	const increment = () => {
		setCounter(counter + 1);
	}

	const decrement = () => {
		setCounter(counter - 1);
	}
	return (
		<div>
			<button onClick={increment}>+</button>
			<button onClick={decrement}>-</button>
			<h1>{counter}</h1>
		</div>
	)
}

export default Counter;