import React from 'react';
import Counter from './components/Counter';

export const Title = () => (
	<h1>
		Introducion a Jest
	</h1>
);

const App = () => {
	return (
		<section>
			<Title />
			<p>
				Lorem ipsum
			</p>
			<section>
				<p>
					Fuera ipsum
				</p>
			</section>
			<h2>Othro titulo</h2>
			<div className='container'>
				<span num={3}>Primero</span>
				{/* <span num='3' active='false' /> */}
			</div>
			<input type='text' />
			<input type="checkbox" />
			<div></div>
			<p>*****</p>
			<p>*****</p>
			<div>
				<p>///////</p>
			</div>
			<Counter />
		</section>
	)
}

export default App;
