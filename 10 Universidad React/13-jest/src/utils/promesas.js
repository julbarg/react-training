export const getDataPromise = () => {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve('Yamile Castro');
		}, 300)
	});
}

export const getDataPromiseError = () => {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			reject('Error');
		}, 300)
	});
}

// { then cathc}
getDataPromise()
	.then((name) => {
		console.log('Name')
	})
	.catch((error) => {
		// manaejar el error
	});

