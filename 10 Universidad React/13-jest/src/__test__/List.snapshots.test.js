import Enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import List from '../components/List';
import React from 'react';

Enzyme.configure({
	adapter: new Adapter()
});

describe('Probando componente <List />', () => {
	test('Validar que coincidad con Snapshot', () => {
		const fruits = [
			{ name: 'fresa', id: 1},
			{ name: 'mango', id: 2},
			{ name: 'banano', id: 3},
			{ name: 'pera', id: 4},
			{ name: 'manzana', id: 5},
		];

		const wrapper = shallow(<List title='Frutas' list={fruits} />);

		expect(toJson(wrapper)).toMatchSnapshot();
	});
})