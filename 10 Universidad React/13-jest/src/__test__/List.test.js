import Enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import List from '../components/List';
import React from 'react';

Enzyme.configure({
	adapter: new Adapter()
});

describe('Probando componente <List />', () => {
	test('Validar nodos', () => {
		const fruits = [
			{ name: 'fresa', id: 1},
			{ name: 'mango', id: 2},
			{ name: 'banano', id: 3},
			{ name: 'pera', id: 4},
			{ name: 'manzana', id: 5},
		];

		const wrapper = shallow(<List title='Frutas' list={fruits} />);

		// Validar si existe el node
		expect(wrapper.find('h1').exists()).toBeTruthy();

		// Validar si tiene clase css
		expect(wrapper.find('h1').hasClass('big')).toBeTruthy();

		// Validar cantidad de elmentos hijos
		expect(wrapper.find('ul').children().length).toBe(5);

		// Validar el contenido de texto
		expect(wrapper.find('li').first().text()).toBe('fresa');
		expect(wrapper.find('li').last().text()).toBe('manzana');
		expect(wrapper.find('ul').childAt(2).text()).toBe('banano');

		// Validar el tipo de nodo / elemento
		expect(wrapper.find('ul').childAt(3).type()).toBe('li');

		// Validar html
		expect(wrapper.find('ul').childAt(0).html()).toBe('<li>fresa</li>');

	});

	test('Validar actualizaciones en props', () => {
		const fruits = [
			{ name: 'fresa', id: 1},
			{ name: 'mango', id: 2},
			{ name: 'banano', id: 3},
			{ name: 'pera', id: 4},
			{ name: 'manzana', id: 5},
		];

		const wrapper = shallow(<List title='Frutas' list={fruits} />);

		// Validar el numero de elmentos li que tiene el componente
		expect(wrapper.find('li').length).toBe(5);

		wrapper.setProps({
			list: [
				{ name: 'fresa', id: 4}
			]
		});

		expect(wrapper.find('li').length).toBe(1);

		wrapper.setProps({
			title: 'Cambio de texto'
		});

		expect(wrapper.find('.big').text()).toBe('Cambio de texto');
	});
})