import { getDataPromise, getDataPromiseError } from '../utils/promesas';

describe('Probando promesas', () => {
	test('Probando getDataPromise', (done) => {
		getDataPromise()
			.then((name) => {
				expect(name).toBe('Yamile Castro');
				done();
			})
			.catch(() => {

			})
	});

	test('Probando getDataPromise con expect', () => {
		// resolves
		return expect(getDataPromise()).resolves.toBe('Yamile Castro');
	});

	test('Probando getDataPromiseError', (done) => {
		getDataPromiseError()
			.catch((error) => {
				expect(error).toBe('Error');
				done();
			})
			.catch(() => {

			})
	});

	test('Probando getDataPromiseError con expect', () => {
		// rejects
		return expect(getDataPromiseError()).rejects.toBe('Error');
	});
})