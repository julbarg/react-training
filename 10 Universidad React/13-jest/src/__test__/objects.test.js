import { createStore } from '../utils/objects';
describe('Probando objects', () => {

	test('Agregar un item en store', () => {
		const store = createStore();

		store.addItem({
			id: 1,
			name: 'Julian',
			country: 'Colombia'
		});

		expect(store.getStore()[0]).toEqual({
			id: 1,
			name: 'Julian',
			country: 'Colombia'
		})
	});

	test('Buscar un item por id', () => {
		const store =createStore();

		store.addItem({
			id: 89,
			name: 'Yamile',
			country: 'Colombia'
		});

		expect(store.getById(89)).toEqual({
			id: 89,
			name: 'Yamile',
			country: 'Colombia'
		});

		expect(store.getById(89)).toMatchObject({
			name: 'Yamile'
		});

		expect(store.getById(89)).toHaveProperty('name');
		expect(store.getById(89)).toHaveProperty('name', 'Yamile');
	})
})