import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import Counter from '../components/Counter';
import toJson from 'enzyme-to-json';

configure({
	adapter: new Adapter()
});

describe('Proband componente <Counter />', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = shallow(<Counter />);
	})
	test('Validar que coincida con Snapshot', () => {
		expect(toJson(wrapper)).toMatchSnapshot();
	});

	test('Validar funcionamiento de botones', () => {
		wrapper.find('button').first().simulate('click');

		expect(wrapper.find('h1').text()).toBe('1');

		wrapper.find('button').last().simulate('click');
		wrapper.find('button').last().simulate('click');
		wrapper.find('button').last().simulate('click');

		expect(wrapper.find('h1').text()).toBe('-2');
	})
})