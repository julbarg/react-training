import {
	suma,
	resta,
	multiplicacion,
	division,
	getRandomNum
} from '../utils/numbers';

describe('Prueba de numbers', () => {
	test('Probando metodo suma', () => {
		expect(suma(5, 5)).toBe(10);
	});

	test('Probando metodo resta', () => {
		expect(resta(5, 5)).toBe(0);
	});

	test('Probando metodo multiplicacion', () => {
		expect(multiplicacion(5, 5)).toBe(25);
	});

	test('Probando metodo division', () => {
		expect(division(5, 5)).toBe(1);
	});

	test('Probando metodo getRandomNum', () => {
		expect(getRandomNum(5, 10)).toBeGreaterThan(4);
		expect(getRandomNum(5, 10)).toBeLessThan(11);
		expect(5).toBeGreaterThanOrEqual(5);
	});
})