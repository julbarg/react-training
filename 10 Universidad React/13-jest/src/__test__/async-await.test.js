import { getDataPromise, getDataPromiseError } from '../utils/promesas';

describe('Probando promesas con async await', () => {
	test('Probando getDataPromise', async () => {
		const name = await getDataPromise();

		expect(name).toBe('Yamile Castro');
	});

	test('Probando getDataPromiseError', async () => {
		try {
			await getDataPromiseError();
		} catch (error) {
			expect(error).toBe('Error');
		}

	});
})