import { getDataCallback } from '../utils/async';

describe('Haciendo test a operaciones asincronas', () => {
	test('Haciendo test a callback', (done) => {
		getDataCallback((name) => {
			expect(name).toBe('Julian Barragan');
			done();
		})
	})
})