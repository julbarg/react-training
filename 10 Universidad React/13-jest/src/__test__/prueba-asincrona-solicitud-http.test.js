import { getUser } from '../utils/prueba-asincrona-solicitud-http';

describe('Probando promesa con solicitud HTTP', () => {

	test('Proband getUsers', async () => {
		const user = await getUser();

		expect(user).toHaveProperty('username', 'Kamren');
		expect(user).toHaveProperty('id');
	});
})