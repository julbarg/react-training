import Enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App, { Title } from '../App';
import React from 'react';

Enzyme.configure({
	adapter: new Adapter()
});

describe('Probando componente <App />', () => {
	test('Probando renderizado de componente', () => {
		const wrapper = shallow(<App />);

		expect(wrapper.html()).toContain('Introducion a Jest');
		// expect(wrapper.find('h1').html()).toBe('<h1>Introducion a Jest</h1>');
		// expect(wrapper.find('h1')).toHaveLength(1);
	});

	test('Diferentes formas para seleccionar elementos o componentes', () => {
		const wrapper = shallow(<App />);

		// find(element)
		console.log(wrapper.find('h2').html());

		// find(selector)
		console.log(wrapper.find('.container').html());
		// Elementos p dentro de un elemento div
		console.log(wrapper.find('div > p').html());
		// Primer elemento hermano despues de un div
		console.log(wrapper.find('div + p').html());
		// Todos los elementos hermanos despues de un div
		console.log(wrapper.find('div ~ p').length);

		console.log(wrapper.find('[num=3]').html());
		// console.log(wrapper.find('[num="3"]').html());
		console.log(wrapper.find('[type="text"]').html());
		console.log(wrapper.find('[type="checkbox"]').html());

		// Acceder a otros components
		console.log(wrapper.find(Title).html());

	});
})