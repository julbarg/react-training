import { createStore } from '../utils/arrays';

describe('Probando la funcionalidad de arrays', () => {

	test('Probando agregar una nueva fruta', () => {
		const store = createStore();

		store.addFruit('fresa');

		expect(store.getFruits()).toStrictEqual(['fresa']);
	});

	test('Probando agregando dos frutas', () => {
		const store = createStore();

		store.addFruit('fresa');
		store.addFruit('melon');

		expect(store.getFruits()).toStrictEqual(['fresa', 'melon']);
	});

	test('Probando si contiene fruta especifica', () => {
		const store = createStore();

		store.addFruit('manzana');
		store.addFruit('pera');

		expect(store.getFruits()).toContain('pera');
		expect(store.getFruits()).not.toContain('banano');
	});

	test('Probando la longitud del array', () => {
		const store = createStore();

		store.addFruit('manzana');
		store.addFruit('pera');

		expect(store.getFruits()).toHaveLength(2);
	});

	test('Probando agregar un objeto con informacion de frutas', () => {
		const store = createStore();

		store.addFruit({
			name: 'manzana',
			price: 78
		});

		expect(store.getFruits()).toContainEqual({
			name: 'manzana',
			price: 78
		});
	});
})