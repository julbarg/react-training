import React from 'react';
import './Image.css'

const Image = () => {
	return (
		<div className='show'>
			<img
				alt='adidas'
				src='https://images.pexels.com/photos/1661470/pexels-photo-1661470.jpeg?cs=srgb&dl=close-up-photo-of-adidas-shoes-1661470.jpg&fm=jpg'
				width='100%'
			 />
		</div>
	)
};

export default Image;