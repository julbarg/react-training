import React from 'react';
import ReactDOM from 'react-dom';
import App from './AppHOCHighOrderComponentThird';

ReactDOM.render(<App />, document.getElementById('root'));
