import { createStore } from 'redux';

// Reducer
// Es una fucion pura que nos regresa el estado actual
const initialState = 0;
const INCREMENT = 'INCREMENT';
const DECREMENT = 'DECREMENT';

// Creadores de acciones
const increment = () => {
	return {
		type: INCREMENT
	}
};

const decrement = () => {
	return {
		type: DECREMENT
	}
};

function counter (state = initialState, action) {
	// if (action.type === 'INCREMENT') {
	// 	return state + 1;
	// }

	// if (action.type === 'DECREMENT') {
	// 	return state - 1;
	// }

	// return state;
	switch (action.type) {
		case INCREMENT:
			return state + 1;

		case DECREMENT:
			return state - 1;

		default:
			return state;
	}
}


// Store
// Almacenamiento de nuestro estado
const store = createStore(counter);

store.subscribe(() => {
	console.log(store.getState());
});

store.dispatch(increment());

store.dispatch(decrement());

setTimeout(() => {
	store.dispatch(increment());
}, 1000);

// setInterval(() => {
// 	store.dispatch({
// 		type:'DECREMENT'
// 	})
// }, 1000);

// store.getState();
// store.dispatch({});
// store.subscribe(fun);

export default store;