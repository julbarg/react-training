import { createStore } from 'redux';

// Reducer
// Es una fucion pura que nos regresa el estado actual
const initialState = 0;
const INCREMENT = 'INCREMENT';
const DECREMENT = 'DECREMENT';

// Creadores de acciones
export const increment = () => {
	return {
		type: INCREMENT
	}
};

export const decrement = () => {
	return {
		type: DECREMENT
	}
};

function counter (state = initialState, action) {
	switch (action.type) {
		case INCREMENT:
			return state + 1;

		case DECREMENT:
			return state - 1;

		default:
			return state;
	}
}

const store = createStore(counter);

export default store;