export const UPDATE_NAME = 'SET_NAME';

export const updateName = (name) => {
	return {
        payload: {
            name
        },
        type: UPDATE_NAME
	}
};