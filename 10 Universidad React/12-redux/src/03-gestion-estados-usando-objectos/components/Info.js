import React from 'react';
import { connect } from 'react-redux';
import { updateName } from '../redux/actions/userActions';

const Info = ({ user, updateName }) => {
    const handlerChange = (e) => {
        const name = e.target.value;

        updateName(name);

    }
    return (
        <div>
            <h1>{ user.name } - { user.country }</h1>
            <input type='text' value={user.name} onChange={handlerChange} />
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        counter: state.counter,
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
	return {
		updateName: (name) => {
            dispatch(updateName(name))
        }

	}
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Info);