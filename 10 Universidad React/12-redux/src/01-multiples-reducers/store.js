import { createStore } from 'redux';
import rootReducer from './reducers';
import { DECREMENT, INCREMENT} from './reducers/counter';

// Creadores de acciones
export const increment = () => {
	return {
		type: INCREMENT
	}
};

export const decrement = () => {
	return {
		type: DECREMENT
	}
};

const store = createStore(rootReducer);

export default store;