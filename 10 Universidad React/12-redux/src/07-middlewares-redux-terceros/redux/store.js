import { applyMiddleware, createStore, combineReducers } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import todo from './reducers/todoReducer';

// Midleware


const confirmDeleteTodo = (store) => (next) => (action) => {
	if (action.type === 'DELETE_TODO') {
		let validation = window.confirm('Desea eliminar el todo');

		validation && next(action);
	} else {
		next(action)
	}


}

const rootReducer = combineReducers({
    todo
});

export default createStore(rootReducer, applyMiddleware(confirmDeleteTodo, logger, thunk));