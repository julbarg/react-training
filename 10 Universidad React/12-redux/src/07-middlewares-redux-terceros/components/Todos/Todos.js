import TodoForm from './TodoForm';
import React from 'react';
import { addTodo, deleteTodo, updateTodo } from '../../redux/actions/todoActions';
import { connect } from 'react-redux';
import { getId } from '../../utils';
import TodoList from './TodoList';

const Todos = ({ todo, addTodo, deleteTodo, updateTodo }) => {
    const handlerSubmit = (e) => {
        e.preventDefault();

        const text = e.target.todo.value;

        addTodo({
            checked: false,
            id: getId(),
            text
        });

        e.target.todo.value = '';
	}

    return (
        <div>
            <h1>Todos</h1>
			<TodoForm onSubmit={handlerSubmit} />
			<TodoList
				todos={todo.todos}
				updateTodo={updateTodo}
				deleteTodo={deleteTodo} />
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        todo: state.todo
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
		addTodo: (todo) => dispatch(addTodo(todo)),
		deleteTodo: (todo) => dispatch(deleteTodo(todo)),
        updateTodo: (todo) => dispatch(updateTodo(todo))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Todos);