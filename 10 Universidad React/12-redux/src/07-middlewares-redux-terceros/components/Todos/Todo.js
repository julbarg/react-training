import React from 'react';

const Todo = ({ todo, deleteTodo, updateTodo }) => (
	<li>
		<input
			type='checkbox'
			checked={todo.checked}
			onChange={() => deleteTodo(todo)}
		/>
		{todo.text}
		<button onClick={() => deleteTodo(todo)}>
			X
		</button>
	</li>
)

export default Todo;