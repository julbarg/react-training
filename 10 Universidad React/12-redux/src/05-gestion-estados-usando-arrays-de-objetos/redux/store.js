import { createStore, combineReducers } from 'redux';
import todo from './reducers/todoReducer';

const rootReducer = combineReducers({
    todo
});

export default createStore(rootReducer);