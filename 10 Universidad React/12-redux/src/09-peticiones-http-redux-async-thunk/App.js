import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import Blog from './components/Blog';
import Counter from './components/Counter';

const App = () => {
	return (
		<Provider store={store}>
			<div>
                <Blog />
				<Counter />
			</div>
		</Provider>
	)
}

export default App;
