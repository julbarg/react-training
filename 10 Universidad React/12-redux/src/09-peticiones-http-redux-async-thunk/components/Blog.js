import { connect } from 'react-redux';
import React from 'react';
import { fetchPosts } from '../redux/actions/postsActions';
import { SyncLoader } from 'react-spinners';

const Blog = (props) => {
	console.log(props)

	const handlerClick = () => {
		props.dispatch(fetchPosts())
	}

	const renderPost = (post) => (
		<li key={post.id}>
			<h2>{ post.title }</h2>
			<p>{ post.body }</p>
		</li>
	)

	return (
		<div>
			<h1>Nuevas entradas de blog</h1>
			<button onClick={handlerClick}>
				Cargar Posts
			</button>
			{ props.blog.isFetching
				? (<SyncLoader />)
				: (
					<ul>
						{ props.blog.posts.map(renderPost)}
					</ul>
				)
			}
		</div>
	)
}

const mapStateToProps = (state) => {
	return state;
}

export default connect(mapStateToProps)(Blog);