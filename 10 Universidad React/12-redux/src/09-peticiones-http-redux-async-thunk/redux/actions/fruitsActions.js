export const ADD_FRUIT = 'ADD_FRUIT';

export const addFruit = (fruit) => ({
    payload: {
        fruit
    },
    type: ADD_FRUIT
})