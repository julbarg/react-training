import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import blog from './reducers/postsReducer';
import counter from './reducers/counterReducer';
import fruits from './reducers/fruitsReducer';
import { composeWithDevTools } from 'redux-devtools-extension';

// Midleware


const confirmDeleteTodo = (store) => (next) => (action) => {
	if (action.type === 'DELETE_TODO') {
		let validation = window.confirm('Desea eliminar el todo');

		validation && next(action);
	} else {
		next(action)
	}


}

const rootReducer = combineReducers({
	blog,
	counter,
	fruits
});

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const composeEnhancers = composeWithDevTools({
	name: 'Redux',
	realtime: true,
	trace: true,
	traceLimit: 20
});

export default createStore(
	rootReducer,
	composeEnhancers(
		applyMiddleware(logger, thunk)
	)
);