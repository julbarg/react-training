import React from 'react';
import { connect } from 'react-redux';
import { increment, decrement } from '../redux/actions/counterActions';

const Counter = ({ increment, decrement, name, counter}) => {
    return (
		<div>
			<button onClick={increment}>+</button>
			<button onClick={decrement}>-</button>
			<h1>{ counter }</h1>
            <p>
                { name }
            </p>
		</div>
	);
}

const mapStateToProps = (state) => {
    return {
        counter: state.counter,
        name: state.user.name
    };
}

const mapDispatchToProps = (dispatch) => {
	return {
		decrement: () => dispatch(decrement()),
		increment: () => dispatch(increment())
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Counter);