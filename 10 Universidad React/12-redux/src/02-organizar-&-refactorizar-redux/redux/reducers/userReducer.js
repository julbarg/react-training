function user (state = { name: 'Julian' }, action) {
	switch (action.type) {
		default:
			return state;
	}
}

export default user;