import Controller from './components/Controller';
import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';

const App = () => {
	return (
		<Provider store={store}>
			<div>
                <Controller />
			</div>
		</Provider>
	)
}

export default App;
