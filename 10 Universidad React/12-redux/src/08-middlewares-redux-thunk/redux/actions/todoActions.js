export const ADD_TODO = 'ADD_TODO';
export const UPDATE_TODO = 'UPDATE:TODO';
export const DELETE_TODO = 'DELETE_TODO';

export const addTodo = (todo) => ({
    payload: todo,
    type: ADD_TODO
})

export const updateTodo = (todo) => ({
    payload: todo,
    type: UPDATE_TODO
});

export const deleteTodo = (todo) => ({
    payload: todo,
    type: DELETE_TODO
});