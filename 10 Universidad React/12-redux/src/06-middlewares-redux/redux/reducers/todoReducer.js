import { ADD_TODO, DELETE_TODO, UPDATE_TODO } from '../actions/todoActions';

const initialState = {
    todos: [
        {
            checked: false,
            id: 'qwe123',
            text: 'Crear componente',
        },
        {
            checked: true,
            id: 'qqasw',
            text: 'Subir video de lecciones',
        }
    ]
}

function todo (state = initialState, action) {
    switch (action.type) {
        case ADD_TODO:
            return {
                ...state,
                todos: [
                    action.payload,
                    ...state.todos
                ]
            }

        case UPDATE_TODO:
            return {
                ...state,
                todos: [
                    ...state.todos.map((todo) => {
                        if (action.payload.id === todo.id) {
                            return {
                                ...todo,
                                checked: !todo.checked
                            };
                        }

                        return todo;
                    })
                ]
            };

        case DELETE_TODO:
            return {
				...state,
				todos: state.todos.filter((todo) => {
					return todo.id !== action.payload.id
				})
			}

        default:
            return state;
    }
}

export default todo;