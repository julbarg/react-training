import { applyMiddleware, createStore, combineReducers } from 'redux';
import todo from './reducers/todoReducer';

// Midleware
const logger = (store) => (next) => (action) => {
	console.log('Ha ocurrido una nueva accion', action)

	setTimeout(() => {
		next(action)
	}, 1000);

	if (action.type !== 'ADD_TODO') {
		// store.diapatch
		setTimeout(() => {
			store.dispatch({
				type: 'ADD_TODO',
				payload: {
					text: 'Todo creado desde Midleware',
					checked: false,
					id: 'ophkhkj'
				}
			})
		}, 4000);
	}
}

const confirmDeleteTodo = (store) => (next) => (action) => {
	if (action.type === 'DELETE_TODO') {
		let validation = window.confirm('Desea eliminar el todo');

		validation && next(action);
	} else {
		next(action)
	}


}

const rootReducer = combineReducers({
    todo
});

export default createStore(rootReducer, applyMiddleware(confirmDeleteTodo, logger));