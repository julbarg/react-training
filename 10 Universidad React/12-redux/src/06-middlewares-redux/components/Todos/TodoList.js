import React from 'react';
import Todo from './Todo';

const TodoList = ({ todos, updateTodo, deleteTodo }) => {

	const renderTodo = (todo) => (
        <Todo
			key={todo.id}
			todo={todo}
			deleteTodo={deleteTodo}
			updateTodo={updateTodo}
		/>
	);

	return (
		<ul>
            { todos.map(renderTodo) }
        </ul>
	)
}

export default TodoList;