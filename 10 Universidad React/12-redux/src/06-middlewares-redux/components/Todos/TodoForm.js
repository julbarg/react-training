import React from 'react';

const TodoForm = ({ onSubmit }) => (
	<form onSubmit={onSubmit}>
		<input
			type='text'
			placeholder='Ingresar todo'
			id='todo'
		/>
		<button>
			Add
		</button>
	</form>
);

export default TodoForm;