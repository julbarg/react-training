import React from 'react';
import ReactDOM from 'react-dom';
import App from './03-gestion-estados-usando-objectos/App';

ReactDOM.render(<App />, document.getElementById('root'));
