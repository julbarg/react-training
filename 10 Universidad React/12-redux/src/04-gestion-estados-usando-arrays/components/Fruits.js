import React from 'react';
import { connect } from 'react-redux';
import { addFruit } from '../redux/actions/fruitsActions';

const Fruits = ({ fruits, addFruit }) => {
    const renderFruit = (fruit, index) => (
        <li key={fruit + index}>
            { fruit }
        </li>
    );

    const handlerSubmit = (e) => {
        e.preventDefault();
        const fruit = e.target.fruit.value;

        e.target.fruit.value = '';

        addFruit(fruit);

    }

    return (
        <div>
            <h1>Fruits</h1>
            <form onSubmit={handlerSubmit}>
                <input
                    type='text'
                    placeholder='Fruit'
                    id='fruit'
                />
                <button>
                    Add
                </button>
            </form>
            <ul>
                {fruits.map(renderFruit)}
            </ul>
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        fruits: state.fruits
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addFruit: (fruit) => dispatch(addFruit(fruit))
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps)
(Fruits);