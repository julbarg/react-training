import { combineReducers } from 'redux';
import fruits from './fruitsReducer';

export default combineReducers({
    fruits
});