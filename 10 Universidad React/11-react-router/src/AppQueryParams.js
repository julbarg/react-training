import './App.css';
import queyString from 'query-string';
import React from 'react';
import { BrowserRouter, Link, NavLink, Route } from 'react-router-dom';

const Hola = () => (
	<h1>Hola!</h1>
);
const Productos = () => (
	<div>
		<h1>Productos</h1>
		<Link to='/productos/hogar'>Hogar</Link>
		<Link to='/productos/electronica'>Electronica</Link>
		<Link to='/productos/gamers'>Gamers</Link>

	</div>
);

const Home = (props) => {
    console.log(props); // props.location.state

    return (
        <h1>Home</h1>
    )
};

const ProductosCategoria = ({ match }) => {
	return (
		<div>
			<h1>Categoria: { match.params.categoria }</h1>
		</div>
	)
};

const Ropa = ({ location }) => {
	let query = new URLSearchParams(location.search);
	let color = query.get('color');
	let talla = query.get('talla');

	query = queyString.parse(location.search);
	color = query.color;
	talla = query.talla;

	const { color2, talla2 } = queyString.parse(location.search);

	return (
		<div>
			<h1>Ropa</h1>
			<p>Color: {color}</p>
			<p>Talla: {talla}</p>

			<p>Color: {color2}</p>
			<p>Talla: {talla2}</p>
		</div>
	);
};

const navStyles = {
    display: 'flex',
    justifyContent: 'space-around'
}

const navActive = {
    color: 'orangered'
};

const Navigation = () => (
    <nav style={navStyles}>
        <NavLink
            to='/'
            exact
            activeStyle={navActive}
        >
            Home
        </NavLink>
        <NavLink
            to='/hola'
            activeClassName='navActive'
        >
            Hola
        </NavLink>
        <NavLink
            to='/productos'
            activeStyle={navActive}
            isActive={(match, location) => {
                if (!match) return false;

                return !match.isExact;
            }}
        >
            Productos
        </NavLink>
		<NavLink
            to='/ropa'
            activeClassName='navActive'
        >
            Ropa
        </NavLink>
    </nav>
)

const App = () => {
	return (
		<BrowserRouter>
            <Navigation />
			<Route path='/' exact component={Home}/>
			<Route path='/hola' render={Hola}/>
			<Route path='/productos' exact render={Productos} />
			<Route path='/productos/:categoria/:id?' render={ProductosCategoria} />
			<Route path='/ropa' component={Ropa}/>
		</BrowserRouter>
	)
}

export default App;
