import React from 'react'
import { BrowserRouter, NavLink, Redirect, Route } from 'react-router-dom';
import './App.css';

const Navigation = () => (
	<nav>
		<NavLink to='/' exact activeClassName='active'>Home</NavLink>
		<NavLink to='/login' activeClassName='active'>Login</NavLink>
		<NavLink to='/perfil' activeClassName='active'>Perfil</NavLink>
	</nav>
);

const isAuth = false;

const Home = () => (
	<h1>Home</h1>
);

const Login = ({ location }) => {
	if (location.state) {
		return <h2>{location.state.message}</h2>
	};

	return (
		<h2>Login</h2>
	)
};

const Perfil = () => {
	return isAuth
		? <h2>Bienvenido a tu perfil</h2>
		: <Redirect to={{
			pathname: '/login',
			state: {
				message: 'Debes hacer login para acceder a esta seccion'
			}
		}} />;
}

const App = () => {
	return (
		<BrowserRouter>
			<Navigation />
			<Route path='/' exact render={Home} />
			<Route path='/login' render={Login} />
			<Route path='/perfil' render={Perfil} />
			<Redirect from='/l' to='/login' />
		</BrowserRouter>
	)
}

export default App;