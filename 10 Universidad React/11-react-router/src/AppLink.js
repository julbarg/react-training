import React from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';

const Hola = () => (
	<h1>Hola!</h1>
);
const Productos = () => (
	<h1>Productos</h1>
);

const Home = (props) => {
    console.log(props); // props.location.state

    return (
        <h1>Home { props.location.state.name } { props.location.state.age }</h1>
    )
};

// const Navigation = () => (
//     <nav>
//         <a href='/'>Home </a>
//         <a href='/hola'>Hola </a>
//         <a href='/productos'>Productos </a>
//     </nav>
// );
const navStyles = {
    display: 'flex',
    justifyContent: 'space-around'
}

const Navigation = () => (
    <nav style={navStyles}>
        <Link to={{
            pathname: '/',
            search: '?orderna=name',
            hash: '#hash-otro',
            state: {
                name: 'Julian',
                age: 29
            }
        }}>Home </Link>
        <Link to='/hola'>Hola </Link>
        <Link to='/productos' replace>Productos </Link>
    </nav>
)

const App = () => {
	return (
		<BrowserRouter>
            <Navigation />
			<Route path='/' exact component={Home}/>
			<Route path='/hola' render={Hola}/>
			<Route path='/productos' render={Productos} />
		</BrowserRouter>
	)
}

export default App;
