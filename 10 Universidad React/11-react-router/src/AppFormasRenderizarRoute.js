import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

const Hola = () => (
	<h1>Hola!</h1>
);
const Productos = () => (
	<h1>Productos</h1>
);

const Home = () => (
	<h1>Home</h1>
)

const App = () => {
	return (
		<BrowserRouter>
			<Route path='/' exact component={Home}/>
			<Route path='/hola' render={() => (<h1>Hola</h1>)}/>
			<Route path='/productos'>
				<Productos />
			</Route>
			<Route path='/productos'>
				{(props) => {
					if (!props.match) return (
						<div>
							*** Wops no coincide con productos
						</div>
					);
					console.log(props); /* {history, location, match, staticCOntext} */
					return (
						<Productos />
					);
				}}
			</Route>
		</BrowserRouter>
	)
}

export default App;
