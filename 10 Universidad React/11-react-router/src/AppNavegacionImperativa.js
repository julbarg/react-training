import React from 'react';
import { BrowserRouter, NavLink, Route } from 'react-router-dom';
import './App.css';

const Navigation = () => (
	<nav>
		<NavLink to='/' exact activeClassName='active'>Home</NavLink>
		<NavLink to='/ninja' activeClassName='active'>Ninja</NavLink>
		<NavLink to='/videos' activeClassName='active'>Videos</NavLink>
	</nav>
);

const Home = () => (
	<h1>Home</h1>
);

const Ninja = () => (
	<h1>Ninja</h1>
);

const Videos = () => (
	<h1>Videos</h1>
);

const NavegacionImperativa = ({ history }) => {
	return (
		<div>
			<button onClick={history.goBack}>
				Atras
			</button>
			<button onClick={history.goForward}>
				Adelante
			</button>
			<button onClick={() => history.go(2)}>
				Go 2
			</button>
			<button onClick={() => history.push('/ninja')}>
				Go Ninja Push
			</button>
			<button onClick={() => history.replace('/videos')}>
				Go Videos Replace
			</button>
		</div>
	);
}

const App = () => {
	return (
		<BrowserRouter>
			<Navigation />
			<Route render={NavegacionImperativa} />
			<Route path='/' exact render={Home} />
			<Route path='/ninja' render={Ninja} />
			<Route path='/videos' render={Videos} />
		</BrowserRouter>
	)
}

export default App;