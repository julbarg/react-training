import './App.css';
import React from 'react';
import { BrowserRouter, Link, NavLink, Route } from 'react-router-dom';

const Hola = () => (
	<h1>Hola!</h1>
);
const Productos = () => (
	<div>
		<h1>Productos</h1>
		<Link to='/productos/hogar'>Hogar</Link>
		<Link to='/productos/electronica'>Electronica</Link>
		<Link to='/productos/gamers'>Gamers</Link>

	</div>
);

const Home = (props) => {
    console.log(props); // props.location.state

    return (
        <h1>Home</h1>
    )
};

const ProductosCategoria = ({ match }) => {
	return (
		<div>
			<h1>Categoria: { match.params.categoria }</h1>
		</div>
	)
};

const navStyles = {
    display: 'flex',
    justifyContent: 'space-around'
}

const navActive = {
    color: 'orangered'
};

const Navigation = () => (
    <nav style={navStyles}>
        <NavLink
            to='/'
            exact
            activeStyle={navActive}
        >
            Home
        </NavLink>
        <NavLink
            to='/hola'
            activeClassName='navActive'
        >
            Hola
        </NavLink>
        <NavLink
            to='/productos'
            activeStyle={navActive}
            isActive={(match, location) => {
                if (!match) return false;

                return !match.isExact;
            }}
        >
            Productos
        </NavLink>
    </nav>
)

const App = () => {
	return (
		<BrowserRouter>
            <Navigation />
			<Route path='/' exact component={Home}/>
			<Route path='/hola' render={Hola}/>
			<Route path='/productos' exact render={Productos} />
			<Route path='/productos/:categoria/:id?' render={ProductosCategoria} />
		</BrowserRouter>
	)
}

export default App;
