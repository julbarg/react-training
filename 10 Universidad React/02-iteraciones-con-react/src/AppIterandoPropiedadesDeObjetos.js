import React, { Component } from 'react';

class App extends Component {
    state = {
        user: {
            name: 'Julian Barragan',
            country: 'Colombia',
            twitter: '@julbarg',
            youtube: 'julbar2008'
        }
    };

    render () {
        const { user } = this.state;
        const keys = Object.keys(user); // => [ 'name', 'country', 'twitter', 'youtube' ]

        return (
            <div>
                <h3>Iterando propiedades de objetos</h3>
                <ul>
                    {keys.map(key => this.renderData(key, user))}
                </ul>
            </div>
        )
    }

    renderData = (key, user) => (
        <li><strong>{ key }:</strong> { user[key] }</li>
    )
}

export default App;