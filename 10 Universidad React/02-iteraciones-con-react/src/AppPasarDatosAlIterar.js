import React, { Component } from 'react';

class App extends Component {
    state = {
        fruits: [
            { name: 'Fresa', price: 5.4 },
            { name: 'Manzana', price: 2.3 },
            { name: 'Sandia', price: 1.2 },
            { name: 'Guayaba', price: 5 },
            { name: 'Pera', price: 4.12 },
            { name: 'Kiwi', price: 3.87 },
            { name: 'Limon', price: 7.21 },
            { name: 'Melon', price: 6.21 },
            { name: 'Cereza', price: 4.98 }
        ],
        frutaSeleccionada: {}
    }

    render () {
        const { fruits } = this.state;

        return (
            <div>
                <ul>
                    { fruits.map(this.renderFruit) }
                </ul>
            </div>
        )
    }

    renderFruit = (fruit, index) => {
        const { frutaSeleccionada } = this.state;

        return (
            <li
                key={index}
                onClick={this.select.bind(this, fruit)}
                style={{
                    color: frutaSeleccionada.name === fruit.name
                        ? 'red'
                        : 'black'
                }}
            >
                { fruit.name } - $ { fruit.price }
            </li>
        );
    }

    select = (frutaSeleccionada, event) => {
        this.setState({
            frutaSeleccionada
        })
    }
}

export default App;