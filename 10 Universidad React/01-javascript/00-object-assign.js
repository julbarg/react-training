const perfil = {
    nombre: 'Julian',
    info: {
        direccion: 'la direccion'
    }
};

const region = {
    pais: 'Colombia',
    info: {
        coordenadas: 'las coordenadas'
    }
};

const social = {
    twitter: '@jubarg'
};

const user = Object.assign(
    {},
    perfil,
    region,
    social,
    {
        info: Object.assign(
            {},
            perfil.info,
            region.info
        )
    }
);

console.log(user);

