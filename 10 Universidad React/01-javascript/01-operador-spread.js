const perfil = {
    nombre: 'Julian',
    info: {
        direccion: 'Avenida Siempreviva'
    }
};

const region = {
    pais: 'Colombia',
    info: {
        coordenadas: 'las coordenadas'
    }
};

const social = {
    twitter: '@julbarg',
    nombre: 'Julian Barragan'
};

const resultado = {
    ...region,
    ...perfil,
    ...social,
    info: {
        ...perfil.info,
        ...region.info
    }
};

console.log(resultado);

const frutasVerdes = [
    'kiwi',
    'uva',
    'limon'
];

const frutasRojas = [
    'manzana',
    'fresa',
    'sandia',
];

const frutas = [
    ...frutasVerdes,
    'pera',
    ...frutasRojas,
    'final'
]

console.log(frutas);

