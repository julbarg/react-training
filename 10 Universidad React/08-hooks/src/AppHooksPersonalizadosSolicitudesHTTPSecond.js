import React, { useState, useEffect } from 'react';
import Header from './Header';

const useFetch = (url, initialState = []) => {
	const [data, setData] = useState(initialState);
	const [isFetching, setFetching] = useState(true);

	useEffect(() => {
		setFetching(true);
		fetch(url)
			.then(res => res.json())
			.then(data => {
				setData(data);
				setFetching(false)
			});
	}, [ url ]);

	return [
		data,
		isFetching
	];
}

const App = () => {
	const [ clicks, setClicks ] = useState(1);
	const [ user, isLoading ] = useFetch('https://jsonplaceholder.typicode.com/users/' + clicks, {});

	const handleClick = () => {
		setClicks(clicks + 1);
	}

	return (
		<div>
			<Header title='Hooks Personalizados' />
			<button onClick={handleClick}>Next User</button>
			{ isLoading && <h3>Loading.... </h3>}
			<h1>{ user.name }</h1>
			<p>{ user.phone }</p>
		</div>
	)
}

export default App;

