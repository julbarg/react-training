import React, { useState, useEffect } from 'react';
import Header from './Header';

const useFetch = (url) => {
	const [data, setData] = useState([]);
	const [isFetching, setFetching] = useState(true);

	useEffect(() => {
		fetch(url)
			.then(res => res.json())
			.then(data => {
				setData(data);
				setFetching(false)
			});
	}, [ url ]);

	return [
		data,
		isFetching
	];
}

const App = () => {
	const [ users, isLoading ] = useFetch('https://jsonplaceholder.typicode.com/users');

	return (
		<div>
			<Header title='Hooks Personalizados' />
			{ isLoading && <h3>Loading.... </h3>}
			<ul>
				{ users.map(item =>
					<li key={item.id}>
						{ item.name }
					</li>
				)}
			</ul>
		</div>
	)
}

export default App;

