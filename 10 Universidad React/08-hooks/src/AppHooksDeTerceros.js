import React, { useState, useEffect } from 'react';
import { useDebounce } from 'use-debounce';

// https://universidad-react-api-test.luxfenix.now.sh/products?name=
const Header = () => {
    const styles = {
        background: 'linear-gradient(20deg, #6813cb, #2575fc)',
        textAlign: 'center',
        borderRadius: '0.2em',
        color: '#FFF',
        padding: '0.3em',
        margin: '0.3em',
        fontSize: '14px'
    }

    return (
        <header style={styles}>
            <h1>
                Hook de terceros
		  <span
                    role='img'
                    aria-label='hook emoji'
                >
                    ⚓
		  </span>
            </h1>
        </header>
    )
}

const App = () => {
    const [ name, setName ] = useState('');
    const [ products, setProducts ] = useState([]);
    const [ nameDebounce ] = useDebounce(name, 1000);

    const handleInput = (e) => {
        setName(e.target.value);
    }

    useEffect(() => {
        // Solicitud HTTP
        fetch('https://universidad-react-api-test.luxfenix.now.sh/products?name=' + name)
            .then(res => res.json())
            .then(data => setProducts(data.products));
    }, [ nameDebounce ]);

    return (
        <div>
            <Header />
            <input
                type='text'
                onChange={handleInput}
            />
            <ul>
                {products.map((product) => (
                    <li key={product.id}>
                        { product.name }
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default App;
