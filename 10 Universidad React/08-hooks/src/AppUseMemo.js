import React, { useState, useMemo } from 'react';
import Header from './Header';

const SuperList = ({ list, log }) => {
    console.log('Render <SuperList /> ' + log, 'color: green');

    return (
        <ul>
            {list.map((item) => (
                <li key={item}>
                    { item }
                </li>
            ))}
        </ul>
    )

}

const App = () => {
    const [clicks, setClicks] = useState(0);

    const add = () => {
        setClicks(clicks => clicks + 1);
    }

    const memoList = useMemo(() => {
        return (
            <SuperList
                list={[ 1, 2 ,4 ,65, 98 ]}
                log='Memorizado'
            />
        );
    }, [])

    return (
        <div>
            <Header title='Hook useMemo'/>
            <button onClick={add}>
                Clicks ({ clicks })
            </button>
            <SuperList
                list={[ 'orange', 'pink', 'purple', 'yellow']}
                log='Normal'
            />
            { memoList }
        </div>
    )
}

export default App;
