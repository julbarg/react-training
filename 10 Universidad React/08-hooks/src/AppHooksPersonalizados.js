import React, { useState, useEffect } from 'react';
import Header from './Header';

const useSize = () => {
    const [width, setWidth] = useState(window.innerWidth);
    const [height, setHeight] = useState(window.innerHeight);

    const handleResize = () => {
        setWidth(window.innerWidth);
        setHeight(window.innerHeight);
    }

    // Agregar Listenr
    useEffect(() => {
        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        }
    }, []);

    return {
        width,
        height
    }
}

const App = () => {
    const { width, height } = useSize();

    return (
        <div>
            <Header title='Hooks Personalizados' />
            <h1>
                width: {width}px height: {height}px
            </h1>
        </div>
    )
}

export default App;
