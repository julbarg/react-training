import React, { useState, useEffect } from 'react';

const Header = () => {
	const styles = {
		background: 'linear-gradient(20deg, #6813cb, #2575fc)',
		textAlign: 'center',
		borderRadius: '0.2em',
		color: '#FFF',
		padding: '0.3em',
		margin: '0.3em',
		fontSize: '14px'
	}

	return (
		<header style={styles}>
			<h1>
				Hook useEffect
		  <span
					role='img'
					aria-label='hook emoji'
				>
					⚓
		  </span>
			</h1>
		</header>
	)
}

// Hooks personalizados
const useHttp = () => {
    const [clicks, setClicks] = useState(0);
}

const App = () => {
    const [ users, setUsers ] = useState([]);
    const [ isFetching, setFetching] = useState(true);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(users => {
                setUsers(users);
                setFetching(false);
            });
    }, []);

    const renderUser = (user) => (
        <li key={user.id}>
            {user.name}
        </li>
    )

    return (
        <div>
            <Header />
                { isFetching && <h1>Loading....</h1> }
                <ul>
                    { users.map(renderUser) }
                </ul>
        </div>
    )
}

export default App;