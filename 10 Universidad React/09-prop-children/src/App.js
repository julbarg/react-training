import React, { Component } from 'react';
import './global.css';

const Parent = ({ children }) => {
    return (
        <div className='box'>
            <div className='box red'>
                { children }
            </div>
            <div className='box green'>
                { children }
            </div>
        </div>
    )
}
class App extends Component {
    render () {
        return (
            <div>
                <Parent>
                    Hijo del texto
                    {`Hola
                        Hola
                            Hola`}
                    <div>Elements</div>
                    {() => {}}
                    {`Template String`}
                    { 999 }
                    { [ 'fresa', 'manzana' ] }
                </Parent>
            </div>
        )
    }
}

export default App;
