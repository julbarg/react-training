import React from 'react';
import { Counter, Title, Button } from './components/Counter';

const App = () => (
    <div>
		<Counter>
			<Title />
			<Title>
				{(click) => (
					<div style={{color: 'green'}}>
						<h1>{ click }</h1>
					</div>
				)}
			</Title>
			<Button type='increment'/>
			<Button type='decrement'/>
		</Counter>
	</div>
);

export default App;
