import React from 'react';
import ReactDOM from 'react-dom';
import App from './AppComposicionImplicita';

ReactDOM.render(<App />, document.getElementById('root'));