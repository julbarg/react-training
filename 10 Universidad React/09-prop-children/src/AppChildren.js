import React, { Component } from 'react';
import './global.css';

const Parent = ({ children: ch }) => {
    const children = React.Children.map(ch, (child, index) => {
        if (index === 0)
            return child;
    });

    React.Children.forEach(ch, (child) => {
        console.log(child);
    });

    const childrenArray = React.Children.toArray(ch);
    console.log(childrenArray);

    const count = React.Children.count(ch);
    console.log(count);

    // const only = React.Children.only(ch);

    return (
        <div className='box'>
            <div className='box blue'>
                { children }
            </div>
        </div>
    )
}
class App extends Component {
    render () {
        return (
            <div>
                <Parent>
                    Hijo del texto
                    {`Hola
                        Hola
                            Hola`}
                    <div>Elements</div>
                    {() => {}}
                    {`Template String`}
                    { 999 }
                    { [ 'fresa', 'manzana' ] }
                </Parent>
            </div>
        )
    }
}

export default App;
