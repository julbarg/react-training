import React, { Component } from 'react';

class InputNoControlado extends Component {
    handleSubmit = (event) => {
        event.preventDefault();
        const nombre = event.target[0].value;
        const email = event.target[1].value;

        this.props.onSend({ nombre, email })
    }

    render () {
        return (
            <form onSubmit={this.handleSubmit}>
                <p>
                    <label htmlFor='name'>Nombre: </label>
                    <input
                        id='name'
                        type="text"
                        placeholder='Nombre'
                    />
                </p>
                <p>
                    <label htmlFor='email'>Email: </label>
                    <input
                        id='email'
                        type="text"
                        placeholder='Email'
                    />
                </p>
                <button>
                    Enviar
                </button>
            </form>
        )
    }
}

class App extends Component {
    send = (data) => {
        console.log(data);
    }

    render () {
        return (
            <div>
                <h1>Usando for en formularios</h1>
                <InputNoControlado
                    onSend={this.send}
                />
            </div>
        )
    }
}

export default App;