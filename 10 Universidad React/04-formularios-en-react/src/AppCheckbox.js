import React, { Component } from 'react';

const Unicorn = () => (
    <span role='img' aria-label='unicornio'>
        🦄
    </span>
);

class App extends Component {
    state = {
        active: false
    }

    hanlderChange = (event) => {
        this.setState({
            active: event.target.checked
        })
    }

    render () {
        const { active } = this.state;

        return (
            <div>
                {active && (
                    <h1>Etiqueta Checkbox <Unicorn /></h1>
                )}
                <form>
                    <input type="checkbox" checked={active} onChange={this.hanlderChange} />
                </form>
            </div>
        )
    }
}

export default App;