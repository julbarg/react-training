import React, { Component } from 'react';

class InputControlado extends Component {
    state = {
        color: 'E8E8E8',
        text: ''
    };

    actualizar = (event) => {
        const text = event.target.value;
        let color = 'green';

        if (text.trim() === '') {
            color = '#E8E8E8'
        }

        if (text.trim() !== '' && text.trim().length < 5) {
            color = 'red';
        }

        this.setState({ color, text });

        this.props.onChange(this.props.name, text);
    }

    render() {
        const styles = {
            border: `1px solid ${this.state.color}`,
            outline: 'none',
            padding: '0.3em 0.6em'
        };

        return (
            <input
                type="text"
                value={this.state.value}
                onChange={this.actualizar}
                placeholder={this.props.placeholder}
                style={styles}
            />
        );
    }
}

class App extends Component {
    state = {
        email: '',
        name: ''
    }

    actualizar = (name, text) => {
        this.setState({
            [name]: text
        });
    }

    render() {
        return (
            <div>
                <h1>Inputs Controlados</h1>
                <InputControlado
                    onChange={this.actualizar}
                    placeholder='Nombre'
                    name='name'
                />
                <InputControlado
                    onChange={this.actualizar}
                    placeholder='Email'
                    name='email'
                />
                <h2>Nombre: {this.state.name}</h2>
                <h2>Email: {this.state.email}</h2>
            </div>
        );
    }
}

export default App;
