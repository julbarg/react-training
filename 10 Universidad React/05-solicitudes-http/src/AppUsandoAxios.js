import axios from 'axios';
import React, { Component } from 'react';

class App extends Component {
    state = {
        isFetching: false,
        movie: {}
    };

    render () {
        return (
            <div>
                <h1>Ejemplo HTTP Buscador de Peliculas</h1>
                <form onSubmit={this.handleSubmit}>
                    <input
                        type='text'
                        placeholder='Nombre de Pelicula'
                    />
                    <button>Buscar</button>
                </form>
                {this.renderMessage()}
                {this.renderMovie()}
            </div>
        );
    }

    renderMessage = () => {
        const { isFetching } = this.state;

        return (
            isFetching && (
                <h2>Cargando</h2>
            )
        )
    }

    renderMovie = () => {
        const { movie, isFetching } = this.state;

        return (
            movie.Title && !isFetching &&
            <div>
                <h1>{movie.Title}</h1>
                <p>{movie.Plot}</p>
                <img
                    src={movie.Poster}
                    alt='Poster'
                    style={{
                        width: '150px'
                    }}
                />
            </div>
        );
    }

    handleSubmit = (event) => {
        event.preventDefault();

        this.setState({ isFetching: true });

        const title = event.target[0].value;
        const url = 'http://www.omdbapi.com/?i=tt3896198&apikey=3b402390';

		axios.get(url, {
			params: {
				t: title
			}
		})
			.then(res => this.setState({
					isFetching: false,
					movie: res.data
			}));
    }
}

export default App;