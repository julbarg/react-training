import React, { Component} from 'react';

class App extends Component {
    state = {
        cargando: true,
        text: '',
        users: []
    };

    componentDidMount () {
        setTimeout(() => {
            this.setState({
                text: 'Hola React'
            });
        }, 1000);

        setTimeout(() => {
            this.setState({
                text: 'Cambio del texto'
            });
        }, 1500);

        fetch('https://jsonplaceholder.typicode.com/users', {method: 'get'})
            .then(res => res.json())
            .then(users => {
                this.setState({ users, cargando: false })
            })
            .catch(error => {
                // Manejo del error
            })
    }

    render () {
        if (this.state.cargando) {
            return <h1>Cargando ......</h1>
        }

        if (this.state.error) {
            return <h1>Ocurrio un error</h1>
        }

        return (
            <div>
                <h1>Solicitudes HTTP en React</h1>
                <h2>{ this.state.text }</h2>
                <ul>
                    { this.state.users.length > 0 && this.state.users.map(this.renderUser) }
                </ul>
            </div>
        );
    }

    renderUser = (user) => (
        <li key={user.id}>
            {user.name + ' '}
            <a href={`http://${user.website}`} target='_blank' rel='noopener noreferrer'>
                { user.website}
            </a>
        </li>
    )
}

export default App;
