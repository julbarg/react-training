import App from './AppAsyncAwaitParaSolicitudesHTTP';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(<App />, document.getElementById('root'));