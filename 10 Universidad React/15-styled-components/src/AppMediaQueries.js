import React from 'react';
import styled from 'styled-components';
import { colorPrincipal } from './colors';


const sizes = {
	mobile: '375px',
	tablet: '768px',
	desktop: '1024px'
};

const devices = {
	mobile: (styles) => {
		return `@media (min-width: ${sizes.mobile}) {
			${styles}
		}`
	},
	tablet: (styles) => {
		return `@media (min-width: ${sizes.tablet}) {
			${styles}
		}`
	},
	desktop: (styles) => {
		return `@media (min-width: ${sizes.desktop}) {
			${styles}
		}`
	}
}

const Header = styled.header`
	background: linear-gradient(20deg, #DB7093, #DAA357);
	background: var(--colorPrincipal);
	background: ${colorPrincipal};
	text-align: center;
	border-radius: 0.2em;
	color: #FFF;
	padding: 0.3em;
	margin: 0.3em;
	font-size: 14px;

	@media (min-width: ${sizes.mobile}) {
		background: #000;

		h1 {
			color: yellow
		}
	}

	@media (min-width: ${sizes.tablet}) {
		background: red;
		font-size: 25px;
		color: #FFF;
	}

	@media (max-width: 1200px) {
		background: purple;
		padding: 1em 0.3em;
	}

	${devices.mobile`
		background: #000;
		font-size: 25px;
		color: yellow;
	`}

	${devices.tablet`
		background: red;
		font-size: 25px;
		color: #FFF;
	`}
`;


const App = () => {
	return (
		<div>
			<Header>
				<h1>Styled Componets - Media queries</h1>
			</Header>
		</div>
	)
}
export default App;
