import React, { useEffect, useState } from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import { colorPrincipal } from './colors';

const Header = styled.header`
	background: linear-gradient(20deg, #DB7093, #DAA357);
	background: var(--colorPrincipal);
	background: ${colorPrincipal};
	text-align: center;
	border-radius: 0.2em;
	color: #FFF;
	padding: 0.3em;
	margin: 0.3em;
	font-size: 14px;
`;


const App = () => {
	return (
		<div>
			<Header>
				<h1>Styled Componets - FUncion de utilidad</h1>
			</Header>
		</div>
	)
}
export default App;
