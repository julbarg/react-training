import React, { useEffect, useState } from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import './style.css';
import { colorPrincipal } from './colors';

const GlobalStyle = createGlobalStyle`
	body {
		@import url('https://fonts.googleapis.com/css?family=Oswald&display=swap');
		font-family: ${props => props.font};
	}
`

// Constante basica
// const colorPrincipal = '#789DFF';

// Constante completa
const paddignBassic = 'padding: 2em';

const sizes = {
	mobile: '375px',
	tablet: '768px',
	desktop: '1024px'
};

const devices = {
	mobile: (styles) => {
		return `@media (min-width: ${sizes.mobile}) {
			${styles}
		}`
	},
	tablet: (styles) => {
		return `@media (min-width: ${sizes.tablet}) {
			${styles}
		}`
	},
	desktop: (styles) => {
		return `@media (min-width: ${sizes.desktop}) {
			${styles}
		}`
	}
}

const Header = styled.header`
	background: linear-gradient(20deg, #DB7093, #DAA357);
	background: var(--colorPrincipal);
	background: ${colorPrincipal};
	text-align: center;
	border-radius: 0.2em;
	color: #FFF;
	padding: 0.3em;
	margin: 0.3em;
	font-size: 14px;
	${paddignBassic};

	@media (min-width: ${sizes.mobile}) {
		background: #000;

		h1 {
			color: yellow
		}
	}

	@media (min-width: ${sizes.tablet}) {
		background: red;
		font-size: 25px;
		color: #FFF;
	}

	@media (max-width: 1200px) {
		background: purple;
		padding: 1em 0.3em;
	}

	${devices.mobile`
		background: #000;
		font-size: 25px;
		color: yellow;
	`}

	${devices.tablet`
		background: red;
		font-size: 25px;
		color: #FFF;
	`}
`;

// Varaibles dinamicas
const getLinearGradient = (rot, color1, color2) => {
	return `linear-gradient(${rot}, ${color1}, ${color2})`;
}

const Header2 = styled.header`
	background: ${getLinearGradient('50deg', 'blue', 'red')};
	text-align: center;
	border-radius: 0.2em;
	color: ${colorPrincipal};
	padding: 0.3em;
	margin: 0.3em;
	font-size: 14px;
	${paddignBassic};
`;



const HeaderEstelizado = styled.header`
	background: linear-gradient(20deg, #DB7093, #DAA357);
	text-align: center;
	border-radius: 0.2em;
	color: #FFF;
	padding: 0.3em;
	margin: 0.3em;
	font-size: 14px;
	${paddignBassic};
	transition: opacity 350ms ease-out;
	opacity: 0.5;

	h1 {
		color: blue;
	}

	div {
		width: 50px;
		height: 50px;
		background: #000;
	}

	.big {
		font-size: 20px;
		color: black;
		text-align: center;
	}

	&:hover {
		opacity: 1;

		h1 {
			color: red;
		}
	}
`;

const Title = styled.h1`
	color: blue;
`;

const Subtitle = styled.h2`
	color: ${colorPrincipal};
`;

const Button = styled.button`
	padding: 0.6em 1.5em;
	background: ${({bg }) => bg || 'gray'};
	${({bg }) => 'background: ' + (bg || 'gray')};
	background: ${({active }) => active ? 'purple' : 'black'};
	border-radius: 0.1em;
	color: #FFF;
	border: 0;
	margin: 0.4em;
	transition: all 350ms ease-out;
`;

// extendiendo styles
const ButtonSpecial = styled(Button)`
	color: gray;
	transition: all 300ms ease-out;

	&:hover {
		transform: scale(1.3)
	}
`;

const Move = ({ className }) => {
	const [ mouseX, setMouseX] = useState(0);

	const handleMove = (e) => {
		setMouseX(e.clientX);
	}

	useEffect(() => {
		window.addEventListener('mousemove', handleMove);

		return () => {
			window.removeEventListener('mousemove', handleMove);
		}
	}, [])
	return (
		<div className={className}>
			{ mouseX }
		</div>
	)
}

// extendiendo estilos a un component ya creado
const MoveStyled = styled(Move)`
	background: yellow;
	font-size: 30px;
	height: 200px;
	text-align: center;
`;

const Input = styled.input.attrs(props => ({
	placeholder: props.placeholder || 'Ingresa el texto',
	type: props.type || 'text'
}))`
	padding: 1em;
	border: 1px solid blue;
`

const App = () => {
	const [active, setactive] = useState(false);

	const toggle = () => setactive(!active);
	return (
		<div>
			<GlobalStyle font="'Oswald', sans-serif"/>
			<Header>
				<h1>Styled Componets</h1>
			</Header>
			<Header2>
				<h1>Variables dinamicas</h1>
			</Header2>
			<HeaderEstelizado>
				<h1>Styled Componets</h1>
				<div>
					+
				</div>
				<div>
					-
				</div>
				<div className='big'>
					Ejemplo
				</div>
			</HeaderEstelizado>
			<Subtitle>
				Ejemplo de titulo
			</Subtitle>
			<Button bg='orange' active>
				Un Boton
			</Button>
			<Button onClick={toggle} active={active}>
				Otro Boton
			</Button>
			<Button>
				Un Boton
			</Button>
			<ButtonSpecial>
				Buton Special
			</ButtonSpecial>

			<Move />
			<MoveStyled />
			<Input placeholder='Hola' />
		</div>
	)
}
export default App;
