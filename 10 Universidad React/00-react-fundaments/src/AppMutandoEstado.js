import React, { Component } from 'react';
class App extends Component {

    state = {
        video: {
            title: 'Super Video',
            likes: 0
        }
    };

    add = () => {
        this.setState((state) => ({
            video: {
                ...state.video,
                likes: state.video.likes + 1
            }
        }))
    }

    render() {

        return (
            <div style={styles}>
                <h1>{ this.state.video.title }</h1>
                <button onClick={this.add}>Likes: ({ this.state.video.likes })</button>
            </div>
        )
    }
}

export default AppMutandoEstado;