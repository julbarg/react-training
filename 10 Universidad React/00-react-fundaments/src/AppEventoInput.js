import React, { Component } from 'react';

class App extends Component {
    state = {
        evento: '',
        text: ''
    }

    manejador = (event) => {
        this.setState({
            evento: event.type,
            text: event.target.value
        })
    }

    render () {
        return (
            <div>
                <input type="text"
                    onChange={this.manejador}
                    onCopy={this.manejador}
                    onPaste={this.manejador}
                />
                <h1>{ this.state.text }</h1>
                <h2>{ this.state.evento}</h2>
            </div>
        );
    }
}

export default App;