import App from './AppPropTypes';
import React from 'react';
import ReactDOM from 'react-dom';
import './destructuracion';

// const name = "Julian"
// const user = {
//     name: 'Julian',
//     age: 29
// };
// const getUser = (user) => {
//     return `Hola ${user.name}. Tengo ${user.age} años`;
// };

ReactDOM.render(<App />, document.getElementById("root"));

