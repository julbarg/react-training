import React, { Component, Fragment } from 'react';

const Computacion = () => (
	<Fragment>
		<li>Monitor</li>
		<li>Mouse</li>
		<li>Teclado</li>
	</Fragment>
);

const Ropa = () => (
	// <Fragment>
	<>
		<li>Playera</li>
		<li>Jenas</li>
		<li>Shorts</li>
	</>
	/* </Fragment> */
);

class App extends Component {
	render() {
		return (
			<div>
				<ul>
					<Computacion />
					<Ropa />
				</ul>
			</div>
		)
	}
};

export default App;