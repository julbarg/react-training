const user1 = {
    name: 'Julian Barragan',
    username: 'julbarg',
    country: 'Colombia',
    social: {
        facebook: 'https://fb....',
        twitter: 'https://tw....'
    }
}

const saluda = (user) => {
    // Super Ninja es un valor por defecto
    // Renombrar los nombres con alias
    const { username: alias, name = 'Super Ninja', social: { twitter: tw }, country } = user;


    const orden = [ 'pizza', 'te verde', 'pie', 12345, false];
    const [ comida, bebida, postre, ...restantes ] = orden;

    console.log(
        `Hola soy ${name}, me dicen ${alias} y vivo en ${country}, mi twitter es ${tw} y me gusta ${comida} con ${bebida} y ${postre}`
    )
    console.log(restantes)

}

saluda(user1);