import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class PortalModal extends Component {
	render() {
		if(!this.props.visible) {
			return null;
		}

		const styles = {
			width: '100%',
			height: '100%',
			position: 'absolute',
			top: '0',
			left: '0',
			background: 'linear-gradient(to top right, #667EEA, #764BA2',
			opacity: 0.95,
			color: '#FFF'
		};

		return ReactDOM.createPortal((
			<div style={styles}>
				{ this.props.children }
			</div>
		), document.getElementById('modal-root'));
	}
}

class App extends Component {
	state = {
		visible: false,
		num: 0
	}

	componentWillMount () {
		setInterval(() => {
			this.setState(state => ({
				num: state.num + 1
			}));
		}, 1000)
	}

	toggle = () => {
		this.setState(state => ({
			visible: !state.visible
		}))
	}

	render() {
		const { visible, num } = this.state;
		return (
			<div>
				<button onClick={this.toggle}>Open</button>
				<PortalModal visible={visible}>
					<button onClick={this.toggle}>Cerrar</button>
					<h1>Hola Portal {num}</h1>
				</PortalModal>
			</div>
		);
	}
}

export default App;