import React, { Component } from 'react';
import './global.css';

class Hijo extends Component {
    state = {
        name: ''
    }

    manejadorClick = () => {
        this.setState(state => ({
            name: 'Julian Barragan'
        }));

        this.props.onSaluda(this.state.name);
    }

    render() {
        return (
            <div className="box blue">
                <h2>Hijo</h2>
                <button
                    onClick={this.manejadorClick}
                >
                    Saluda
                </button>
                <h1>
                    { this.state.name }
                </h1>
            </div>
        );
    }
}
class App extends Component {
    manejador = (name) => {
        alert(name);
    }

    render() {
        return (
            <div className="box red">
                <Hijo
                    onSaluda={this.manejador}
                />
            </div>
        );
    }
}

export default App;