import React, { Component } from 'react';

const Saludo = (props) => {
    return (
        <div>
            <div>
                { props.name && <strong>{props.name}</strong>}
            </div>
            { props.saluda ? ( <h1>Renderizando Saludo</h1>) : (<p>No saluda</p>)}
        </div>
    )
}

class App extends Component {
    render() {
        return (
            <div>
                <Saludo saluda name="Julian" />
            </div>
        );
    }
}

export default App;