import React, { Component } from 'react';

class AppEventoSintetico extends Component {

    // evento estandarizado por parte de React
    manajedor = (e) => {
        e.preventDefault();
        console.log(e.nativeEvent);
    }

    render () {
        return (
            <div>
                <a
                    href="https://google.com"
                    onClick={this.manajedor}
                >
                    Google
                </a>
            </div>
        )
    }
}

export default AppEventoSintetico;