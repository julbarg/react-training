import React, { Component } from 'react';

class PersistenciaEventos extends Component {
    state = {
        color: 'blue'
    }

    hanlderChange = (event) => {
        // event.persist();
        const color = event.target.value

        this.setState(state => ({
            color
        }));
    }

    render () {
        return (
            <div>
                <input
                    type="text"
                    onChange={this.hanlderChange}
                />
                <h1 style={{
                    color: this.state.color
                }}>
                    { this.state.color }
                </h1>
            </div>
        );
    }
}

const App = () => (
    <div>
        <PersistenciaEventos />
    </div>
)

export default App;