import React, { Component } from 'react';
import PropTypes from 'prop-types';

const Comp = () => (
	<div></div>
)

Comp.propTypes = {}
Comp.defaultProps = {}

const noop = () => {};

class Profile extends Component {
	static propTypes = {
		name: PropTypes.string.isRequired,
		bio: PropTypes.string.isRequired,
		email: PropTypes.string,
		age: PropTypes.number
	}

	static defaultProps = {
		name: 'Julian Barragan',
		onHello: noop
	}

	saluda = () => {
		this.props.onHello();
	}

	render () {
		const { name, bio, email } = this.props;

		return (
			<div>
				<h1>{ name }</h1>
				<p>{ bio }</p>
				<a href={`mailto:${email}`}>
					{email}
				</a>
				<button onClick={this.saluda}>Hello</button>
			</div>
		)
	}
}

// Profile.propTypes = {} funcionales o basados en clases
// Profile.defaultProps = {}

class App extends Component {
	render() {
		return (
			<div>
				<Profile 
					bio="Ingeniero de Sistemas"
					email="julbarg@gmail.com"
					// onHello={() => {alert('Sample')}}
				/>

			</div>
		);
	}
}

export default App;