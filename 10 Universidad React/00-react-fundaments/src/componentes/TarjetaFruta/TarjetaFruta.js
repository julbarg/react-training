import React, { Component } from 'react';
import styles from './TarjetaFruta.module.css';

class TarjetaFruta extends Component {
    constructor () {
        super();

        this.state = {
            cantidad: 0
        }
    }

    agregar = () => this.setState({
        cantidad: this.state.cantidad + 1
    });

    quitar = () => this.setState({
        cantidad: this.state.cantidad - 1
    });

    limpiar = () => this.setState({
        cantidad: 0
    });

    render () {
        const hasItems = this.state.cantidad > 0;
        const clases = `${styles.TarjetaFruta} ${hasItems ? styles['TarjetaFruta-activa'] : '' }`;
        // const styles = {
        //     border: '1px solid black',
        //     marginBottom: '1em',
        //     borderRadius: '0.5em',
        //     padding: '1em',
        //     background: hasItems > 0 ? 'linear-gradient(45deg, black, #4A02F7)' : '#FFF',
        //     color: hasItems > 0 ? '#FFF' : '#000',
        //     transition: 'all 400ms ease-out'
        // };

        return (
            <div className={clases}>
                <h3>{ this.props.name }</h3>
                <div>Cantidad: {this.state.cantidad}</div>
                <button onClick={this.agregar}> + </button>
                <button onClick={this.quitar}> - </button>
                <button onClick={this.limpiar}> 0 </button>
                <hr />
                <p>${ this.props.price }</p>
                <p>Total: { this.props.price * this.state.cantidad }</p>
            </div>
        );
    }
}

export default TarjetaFruta;