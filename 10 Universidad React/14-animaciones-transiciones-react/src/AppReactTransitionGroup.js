import React, { useState } from 'react';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import './AppReactTransitionGroup.css'


const App = () => {
	const [count, setCount] = useState(0);

	return (
		<div>
			<button onClick={() => setCount(count + 1)}>
				+
			</button>
			<button onClick={() => setCount(count - 1)}>
				-
			</button>
			<div className='box'>
				<TransitionGroup>
					<CSSTransition
						timeout={1000}
						classNames='fade'
						key={count}
					>
						<h1>
							{count}
						</h1>
					</CSSTransition>
				</TransitionGroup>
			</div>
		</div>
	);
}

export default App;