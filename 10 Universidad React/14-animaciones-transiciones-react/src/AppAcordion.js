import React from 'react';
import Acordion from './components/Acordion';


const App = () => {
	return (
		<div>
			<Acordion
				title='Ejemplo de Acordion'
				content='Lorem ipsim In dolor eiusmod exercitation Lorem nisi veniam laboris sunt aliqua qui.'
				bgColor='#000'
			/>

			<Acordion
				title='Porque universidad React'
				content='Voluptate aute occaecat dolor amet.Eiusmod magna ut ad quis ex ad sunt reprehenderit occaecat adipisicing ut magna. Id laboris tempor non magna nulla minim excepteur duis laborum. Ea anim incididunt Lorem laborum.'
				bgColor='orangered'
			/>

			<Acordion
				title='Ejemplo de Acordion'
				content='Lorem ipsim In dolor eiusmod exercitation Lorem nisi veniam laboris sunt aliqua qui.'
			/>
		</div>
	)
}

export default App;