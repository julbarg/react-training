import React, { useState, useRef } from 'react';
import propTypes from 'prop-types';
import arrowImg from '../img/arrow.svg';

const Acordion = ({ title, content, bgColor }) => {
	const [isExpanded, setExpanded] = useState(false);
	const contentRef = useRef();


	const panelStyles = {
		background: bgColor,
		color: '#FFF',
		padding: '0.5em 1em',
		display: 'flex',
		alignItem: 'center',
		justifyContent: 'space-between',
		userSelect: 'none'
	}

	const contentStyles = {
		height: isExpanded ? contentRef.current.scrollHeight : '0',
		overflow: 'hidden',
		transition: 'all 350ms ease-out',
		border: '1px solid ' + bgColor,
		padding: isExpanded ? '1em 0.5em' : '0 0.5em'
	}

	const toggle = () => {
		setExpanded(!isExpanded);
	}

	const imageStyles = {
		width: '18px',
		transform: isExpanded ? 'rotate(90deg)' : 'rotate(0)',
		transition: 'transform 250ms ease'
	}

	return (
		<div>
			<div
				onClick={toggle}
				style={panelStyles}
			>
				<span>{title}</span>
				<img
					src={arrowImg}
					alt='Arrow'
					style={imageStyles}
				/>
			</div>
			<div
				style={contentStyles}
				ref={contentRef}
			>
				{ content }
			</div>
		</div>
	)
}

Acordion.defaultProps = {
	title: '',
	content: '',
	bgColor: '#523DA5'
}

Acordion.propTypes = {
	title: propTypes.string,
	content: propTypes.string,
	bgColor: propTypes.string
}

export default Acordion;