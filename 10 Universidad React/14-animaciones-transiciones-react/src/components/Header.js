import React from 'react';

const Header = ({ show, title }) => {

	const activeStyles = {
		background: '#3d1f9a',
		transform: 'scale(1)',
		color: '#FFF'
	}

	let headerStyles = {
		background: 'black',
		transform: 'scale(0)',
		position: 'absolute',
		textAlign: 'center',
		borderRadius: '.4em',
		color: 'orange',
		padding: '0.5em',
		margin: '0.5em',
		fontSize: '14px',
		transition: 'all 800ms ease'
	}

	if (show) {
		headerStyles = {
			...headerStyles,
			...activeStyles
		}
	}

	return (
		<header style={headerStyles}>
			<h1>
				{ title }
		  <span role='img' aria-label='fire'>
					🔥
		  </span>
			</h1>
		</header>
	)
}

export default Header;