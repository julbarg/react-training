import React, { useState, useEffect } from 'react';
import './Slides.css';
import propTypes from 'prop-types';

const Slides = ({ images, interval }) => {
	const [activeIndex, setActiveIndex] = useState(0);

	useEffect(() => {
		const tick = setInterval(() => {
			if (activeIndex < images.length - 1) {
				setActiveIndex(activeIndex + 1);
			} else {
				setActiveIndex(0);
			}
		}, interval);

		return () => clearInterval(tick);
	}, [activeIndex, images.length, interval ]);

	const renderImage = (image, index) => {
		return (
			<img
				src={image.src}
				alt={image.src}
				className={
					index === activeIndex
						? 'Slide_Container_Img animaShow'
						: 'Slide_Container_Img animaHide'
				}
				key={image.src}
			/>
		);
	}

	return (
		<div className='Slide'>
			<div className='Slide_Container'>
				{images.map(renderImage)}
				<div className='Slide_Container_Title'>
					{ images[activeIndex].title }
				</div>
			</div>
		</div>
	)
}

Slides.propTypes = {
	interval: propTypes.number,
	images: propTypes.arrayOf(
		propTypes.shape({
			src: propTypes.string.isRequired,
			title: propTypes.string.isRequired
		})
	)
}

Slides.defaultProps = {
	images: [],
	interval: 5000
}

export default Slides;