import React from 'react';
import './Animations.css';

const Header = ({ show, title }) => {

	const clases = show
		? 'header header-active'
		: 'header';

	return (
		<header className={clases}>
			<h1>
				{ title }
		  		<span role='img' aria-label='fire'>
					🔥
		  		</span>
			</h1>
		</header>
	)
}

export default Header;