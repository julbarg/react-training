import React, { useState } from 'react';
import Header from './components/HeaderAnimation';

const App = () => {
	const [active, setActive] = useState(false)

	const toggle = () => setActive(!active)

	return (
		<div>
			<button onClick={toggle}>
				{active ? 'Desactivar' : 'Activar'}
			</button>
			<Header show={active} title='Transiciones CSS en linea'/>
		</div>
	)
}
export default App