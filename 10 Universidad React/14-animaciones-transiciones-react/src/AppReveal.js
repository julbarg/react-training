import React from 'react';
import Zoom from 'react-reveal/Zoom';
import Bounce from 'react-reveal/Bounce';
import Rotate from 'react-reveal/Rotate';

const App = () => {
	return (
		<div>
			<section>
				<h3>Ejemplo de titulo</h3>
				<p>
					Excepteur commodo voluptate velit cillum occaecat eiusmod magna Lorem sint cillum magna consectetur. Id esse minim fugiat ad enim velit labore aliqua. Aliquip anim commodo dolor ex aliqua ipsum officia ullamco mollit sit eiusmod ex in.
					Exercitation ad enim ipsum dolor consequat. Reprehenderit deserunt amet eiusmod cillum cupidatat. Lorem officia officia anim nulla ex commodo id culpa officia culpa commodo. Et ea eiusmod dolore laboris ut exercitation est excepteur veniam consequat proident.
				</p>
			</section>
			<section>
				<h3>Ejemplo de titulo</h3>
				<p>
					Excepteur commodo voluptate velit cillum occaecat eiusmod magna Lorem sint cillum magna consectetur. Id esse minim fugiat ad enim velit labore aliqua. Aliquip anim commodo dolor ex aliqua ipsum officia ullamco mollit sit eiusmod ex in.
					Exercitation ad enim ipsum dolor consequat. Reprehenderit deserunt amet eiusmod cillum cupidatat. Lorem officia officia anim nulla ex commodo id culpa officia culpa commodo. Et ea eiusmod dolore laboris ut exercitation est excepteur veniam consequat proident.
				</p>
			</section>
			<Zoom>
				<section>
					<h3>Ejemplo de titulo</h3>
					<p>
						Excepteur commodo voluptate velit cillum occaecat eiusmod magna Lorem sint cillum magna consectetur. Id esse minim fugiat ad enim velit labore aliqua. Aliquip anim commodo dolor ex aliqua ipsum officia ullamco mollit sit eiusmod ex in.
						Exercitation ad enim ipsum dolor consequat. Reprehenderit deserunt amet eiusmod cillum cupidatat. Lorem officia officia anim nulla ex commodo id culpa officia culpa commodo. Et ea eiusmod dolore laboris ut exercitation est excepteur veniam consequat proident.
					</p>
				</section>
			</Zoom>
			<section>
				<h3>Ejemplo de titulo</h3>
				<p>
					Excepteur commodo voluptate velit cillum occaecat eiusmod magna Lorem sint cillum magna consectetur. Id esse minim fugiat ad enim velit labore aliqua. Aliquip anim commodo dolor ex aliqua ipsum officia ullamco mollit sit eiusmod ex in.
					Exercitation ad enim ipsum dolor consequat. Reprehenderit deserunt amet eiusmod cillum cupidatat. Lorem officia officia anim nulla ex commodo id culpa officia culpa commodo. Et ea eiusmod dolore laboris ut exercitation est excepteur veniam consequat proident.
				</p>
			</section>
			<Bounce>
				<section>
					<h3>Ejemplo de titulo</h3>
					<p>
						Excepteur commodo voluptate velit cillum occaecat eiusmod magna Lorem sint cillum magna consectetur. Id esse minim fugiat ad enim velit labore aliqua. Aliquip anim commodo dolor ex aliqua ipsum officia ullamco mollit sit eiusmod ex in.
						Exercitation ad enim ipsum dolor consequat. Reprehenderit deserunt amet eiusmod cillum cupidatat. Lorem officia officia anim nulla ex commodo id culpa officia culpa commodo. Et ea eiusmod dolore laboris ut exercitation est excepteur veniam consequat proident.
					</p>
				</section>
			</Bounce>
			<Rotate>
				<section>
					<h3>Ejemplo de titulo</h3>
					<p>
						Excepteur commodo voluptate velit cillum occaecat eiusmod magna Lorem sint cillum magna consectetur. Id esse minim fugiat ad enim velit labore aliqua. Aliquip anim commodo dolor ex aliqua ipsum officia ullamco mollit sit eiusmod ex in.
						Exercitation ad enim ipsum dolor consequat. Reprehenderit deserunt amet eiusmod cillum cupidatat. Lorem officia officia anim nulla ex commodo id culpa officia culpa commodo. Et ea eiusmod dolore laboris ut exercitation est excepteur veniam consequat proident.
					</p>
				</section>
			</Rotate>
		</div>
	)
}

export default App;