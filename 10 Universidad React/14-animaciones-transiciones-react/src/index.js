import React from 'react';
import ReactDOM from 'react-dom';
import App from './AppReveal';

ReactDOM.render(<App />, document.getElementById('root'));
