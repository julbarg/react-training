import React, { Component } from 'react';


class Timer extends Component {
	state = {
		time: 0,
		isPlaying: true
	}

	tick = null;

	play = () => {
		this.setState({ isPlaying: true });

		this.tick = setInterval(() => {
			this.setState(state => ({
				time: state.time + 1
			}));
		}, 1000);
	}

	pause = () => {
		this.setState({ isPlaying: false });
		clearInterval(this.tick);
	}

	toggle = () => {
		if (this.state.isPlaying) {
			this.pause();
		} else {
			this.play();
		}
	}

	render () {
		const { time, isPlaying } = this.state;

		return (
			<div>
				<h1>{ time }</h1>
				<button onClick={this.toggle}>
					{ isPlaying ? 'Pause' : 'Play' }
				</button>
			</div>
		);
	}

	componentDidMount() {
		this.play();
	}

	componentWillUnmount () {
		console.log('Desmontando');
		clearInterval(this.tick);
		this.props.onDestroy();
	}
}
class App extends Component {
	state = {
		mostrar: true,
		mensaje: ''
	};

	desmontar = () => {
		this.setState({
			mostrar: false
		});
	}

	handleDestroy = () => {
		this.setState({
			mensaje: 'El component contador fue desmontado'
		})
	}

	render () {
		return (
			<div>
				<h1>componentWillUnmount</h1>
				<button onClick={this.desmontar}>
					Desmontar
				</button>
				{ this.state.mostrar && <Timer onDestroy={this.handleDestroy} /> }
				<h4>{ this.state.mensaje }</h4>
			</div>
		)
	}
}

export default App;