import React, { Component } from "react";

class Contador extends Component {
    state = {
        num: this.props.num
	};
	
	static getDerivedStateFromProps (nextProps, prevState) {
		if (prevState.num < nextProps.num) {
			return {
				num: nextProps.num
			}
		}
	}

    agregar = () => {
        this.setState(state => ({
			num: state.num + 1
		}));
	}

	render () {
		return (
			<div>
				<hr />
				<button onClick={this.agregar}>Click ({this.state.num})</button>
			</div>
		)
	}
}

class App extends Component {
	state = {
		numero: 0
	};

	handlerChange = (event) => {
		let numero = parseInt(event.target.value);

		if (isNaN(numero)) {
			numero = 0;
		}

		this.setState({ numero })
	}
    render () {
		const { numero } = this.state;

        return (
            <div>
                <h1>getDeliveryStateFromProps</h1>
				<h2>{ numero }</h2>
				<input type='text' onChange={this.handlerChange} />
                <Contador
                    num={numero}
                />
            </div>
        )
    }
}

export default App;