import React, { Component } from 'react';

class UserDetails extends Component {
    state = {
        isFetching: false,
        user: {}
    }

    componentDidMount () {
        this.fetchData();
    }

    componentDidUpdate (prevProvs, prevState) {
        if (prevProvs.userId !== this.props.userId) {
            this.fetchData();
        }
    }

    fetchData = () => {
        this.setState(this.setState({ isFetching: true }));

        fetch(`https://jsonplaceholder.typicode.com/users/${this.props.userId}`)
            .then(res => res.json())
            .then(user => this.setState({
                isFetching: false,
                user
            }));
    }

    render () {
        return (
            <div>
                { this.state.isFetching ?
                    this.renderMessage() :
                    this.renderUser()
                }
            </div>
        );
    }

    renderMessage = () => (<h2>Loading.....</h2>);

    renderUser = () => (
        <React.Fragment>
            <h2>Users Details</h2>
            <pre>
                { JSON.stringify(this.state.user, null, 4)}
            </pre>
        </React.Fragment>
    );
}

class App extends Component {
    state = {
        id: 1
    }

    aumentar = () => {
        this.setState(state => ({
            id: state.id + 1
        }));
    }

    render () {
        const { id } = this.state;

        return (
            <div>
                <h1>ComponentDidUpdate</h1>
                <h2>ID {this.state.id}</h2>
                <button onClick={this.aumentar}>Aumentar</button>
                <UserDetails
                    userId={id}
                />
            </div>
        )
    }
}

export default App;