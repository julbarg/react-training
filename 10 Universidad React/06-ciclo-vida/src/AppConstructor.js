import React, { Component } from 'react';

class Contador extends Component {
	constructor (props ) {
		super(props);

		this.state = {
			num: props.num
		}

		this.agregar = this.agregar.bind(this);

		this.title = React.createRef();
	}

	agregar () {
        console.log(this.title);

		this.setState(state => ({
			num: state.num + 1
		}));
	}

	render () {
		return (
			<div>
				<h2 ref={this.title}>
					{ this.state.mensaje }
				</h2>
				<button onClick={this.agregar}>Click ({this.state.num})</button>
			</div>
		)
	}
}

class App extends Component {
	render () {
		return (
			<div>
				<h1>Metodo constructor</h1>
				<Contador num={5} />
				<Contador num={200} />
			</div>
		)
	}
}

export default App;
