import React, { Component } from 'react';

class App extends Component {
	state = {
		num: 0
	};

	shouldComponentUpdate (nextProps, nextState) {
		if (nextState.num <= 5) {
			return true;
		}

		return false;
	}

	aumentar = () => {
		this.setState(state => ({
			num: state.num + 1
		}));
	}

	render () {
		return (
			<div>
				<h1>shouldComponentUpdate</h1>
				<button onClick={this.aumentar}>
					Clicks ({ this.state.num })
				</button>
			</div>
		)
	}
}

export default App;