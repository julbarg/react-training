import React, { Component } from 'react';

class App extends Component {
    state = {
        text: 'Hola'
    }

    title = React.createRef();

    getSnapshotBeforeUpdate () {
        console.log(this.title.current.innerText);
        return 'Hola desde getSnapshotBeforeUpdate';
    }

    componentDidUpdate (prevProps, prevState, snapshot) {
        console.log(this.title.current.innerText);
        console.log(snapshot);
    }

    dispatch = () => {
        this.setState({
            text: 'Adios Bye'
        });
    }

    render () {
        return (
            <div>
                <h1>getSnapshotBeforeUpdate</h1>
                <h2 ref={this.title}>
                    { this.state.text}
                </h2>
                <button onClick={this.dispatch}>
                    DISPATCH
                </button>
            </div>
        );
    }
}

export default App;