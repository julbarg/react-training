import React from 'react';
import ReactDOM from 'react-dom';
import App from './AppComponentWillUnmount';

ReactDOM.render(<App />, document.getElementById('root'));
