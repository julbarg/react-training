import React, { Component } from 'react';

class Http extends Component {
    state = {
        photos: []
    }

    componentDidMount () {
        fetch('https://jsonplaceholder.typicode.com/photos')
            .then(res => res.json())
            .then(photos => this.setState({ photos }));
    }
    render () {
        const { photos } = this.state;
        return (
            <div>
                { console.log(photos)}
                { photos
                    .filter(photo => photo.id < 10)
                    .map(photo => (
                        <img
                            key={photo.id}
                            src={photo.thumbnailUrl}
                            alt={photo.title}
                        />
                )) }
            </div>
        );
    }
}

class Events extends Component {
    state = {
        width: window.innerWidth
    }

    componentDidMount () {
        window.addEventListener('resize', this.handlerRize)
    }

    handlerRize = () => {
        console.log('Evento disparado');
        console.log(window.innerWidth);
        this.setState({ width: window.innerWidth })
    }

    render () {
        return (
            <div>
                <h2>Events</h2>
                <h3>{ this.state.width }</h3>
            </div>
        )
    }
}

class App extends Component {
    render () {
        return (
            <div>
                <h1>ComponentDidMount</h1>
                <Http />
                <Events />
            </div>
        )
    }
}

export default App;