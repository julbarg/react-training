import React, { Component } from 'react';

const boton = (
    <button>Boton</button>
);

class App extends Component {

    render () {
        return (
            <div>
                <h1>Metodo Render</h1>
                { this.renderList() }
                { boton }
            </div>
        );
    }

    render () {
        return [
            <li key='one'>One</li>,
            1,
            'Hola'
        ];
    }

    render () {
        return 'Hola soy un text';
    }

    render () {
        if (true) {
            return null;
        }

        return (
            <h1>Hey</h1>
        );
    }

    renderList = () => {
        return (
            <ul>
                <li>Fresa</li>
                <li>Sandia</li>
                <li>Mango</li>
            </ul>
        )
    }
}

export default App;