import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import React from 'react';
import styles from './Burger.module.css';

const Burger = (props) => {
    let transformIngredients = Object.keys(props.ingredients)
        .map(idKey => {
            return [...Array(props.ingredients[idKey])].map((_, i) =>
                <BurgerIngredient key={idKey + i} type={idKey} />
            )
        })
        .reduce((arr, el) => {
            return arr.concat(el);
        }, []);

    if (transformIngredients.length === 0) {
        transformIngredients = <p>Please start adding ingredients!</p>
    }

    return (
        <div className={styles.Burger}>
            <BurgerIngredient type="bread-top"/>
            {transformIngredients}
            <BurgerIngredient type="bread-bottom"/>
        </div>
    );
}

export default Burger;