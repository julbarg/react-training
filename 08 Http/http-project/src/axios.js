import axios from 'axios';

const intances = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com'
});

intances.defaults.headers.common['Authorization'] = 'AUTH_TOKEN FROM INSTANCES';

export default intances;